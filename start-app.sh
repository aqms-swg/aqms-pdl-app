#!/bin/bash


# With aqms-pdl-app installed as python module, this should work from anywhere:
aqms-pdl-app --configfile ../aqms-pdl-app/config.loc --host 127.0.0.1 --port 5001 >> log.txt 2>&1 &

# Alternatively you can set an ENV var to find DB params:
export AQMS_DB_CONFIG=work/configs/config.yml
aqms-pdl-app --loglevel DEBUG --maxrows 21 >> log.txt 2>&1 &
