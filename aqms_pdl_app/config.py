
class Config(object):

    # DEFAULTS
    # These can be overwritten by:
    # 1. cmd-line: e.g., --maxrows 500
    # 2. flask-aqms: e.g., param['MAXROWS'] = 14
    # 3. config.yml: e.g.,
    #   flask:
    #      MAXROWS: 300
    MAXDAYS = 7
    MAXROWS = 5
    LOG_LEVEL = "INFO"
    LOG_TYPE = "watched"
    LOG_DIR = "log"
    #WWW_LOG_NAME = "localhost:5000"
    #SEND_FILE_MAX_AGE_DEFAULT = 0
    #SQLALCHEMY_TRACK_MODIFICATIONS = False
    #SQLALCHEMY_DATABASE_URI = None

    @classmethod
    def set_args(cls, **kwargs):

        for field in kwargs.keys():
            #print("set_args: set field=%s to val=%s" % (field, kwargs[field]))
            setattr(cls, field, kwargs[field])

