from obspy import UTCDateTime

"""
    The functions in here create the html/css pages returned by the app
"""

html_head = """
<!DOCTYPE html>
<head>
  <link rel="stylesheet" href="/static/styles/styles.css">
  <link rel="stylesheet" href="/static/styles/table_styles.css">
  <link rel="stylesheet" href="/static/styles/nav_styles.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
  <script src="/static/styles/sorttable.js"></script>
<!--
-->
  <title>AQMS PDL App</title>
</head>
"""

def make_events_table(events):
    return_str = html_head
    return_str += '''
<body>
<div class="topnav">
    <a href="/">AssocEvents</a>
    <a class="active" href="events">Events</a>
    <a href="eventprefmag">Eventprefmag</a>
    <a href="filters">Filters</a>
    <a href="magprefpriority">Magprefpriority</a>
</div>
<form action="" method="post">

<!-- table class="event-table sortable" -->
<!-- <table class="sortable"> -->
<table class="event-table sortable">
  <thead>
    <tr>
      <th class="CellWithCommentBelow">evid<span class="CellCommentBelow">event id</span></th>
      <th class="CellWithCommentBelow">auth<span class="CellCommentBelow">event.auth</span></th>
      <th class="CellWithCommentBelow">subsrc<span class="CellCommentBelow">event.subsource</span></th>
      <th></th>
      <th class="CellWithCommentBelow">origin time<span class="CellCommentBelow">origin time</span></th>
      <th class="CellWithCommentBelow">lat<span class="CellCommentBelow">origin latitude</span></th>
      <th class="CellWithCommentBelow">lon<span class="CellCommentBelow">origin longitude</span></th>
      <!--<th>dep</th>-->
      <th class="CellWithCommentBelow">mag<span class="CellCommentBelow">event magnitude</span></th>
      <th class="CellWithCommentBelow">F<span class="CellCommentBelow">event.selectFlag</span></th>
      <th class="CellWithCommentBelow">ver<span class="CellCommentBelow">event.version</span></th>
    </tr>
  </thead>
  <tbody>
 '''

    #if 1:
        #ii=0
        #event = events[ii]

    for ii, event in enumerate(events):

        rid = "evid-%d" % event.evid
        child_rid = "%s_origins" % rid

        if (ii%2) == 0:
            return_str += "<tr id='%s' class='hover' name='%s' onclick=\"toggle('%s');\">\n" % (rid, rid, child_rid)
        else:
            return_str += "<tr id='%s' class='hover even' name='%s' onclick=\"toggle('%s');\">\n" % (rid, rid, child_rid)

        return_str += "  <td>%d</td>\n" % event.evid
        return_str += "  <td>%s</td>\n" % event.auth
        return_str += "  <td>%s</td>\n" % event.subsource
        return_str += "  <td></td>\n"
        return_str += "  <td>%s</td>\n" % UTCDateTime(event.preferred_origin.datetime).strftime('%Y-%m-%dT%H:%M:%S.%f')[:-4]
        return_str += "  <td>%7.3f</td>\n" % event.preferred_origin.lat
        return_str += "  <td>%8.3f</td>\n" % event.preferred_origin.lon
        #return_str += "  <td>%6.2f</td>\n" % event.preferred_origin.depth

        if getattr(event, 'preferred_magnitude', None):
            mag = "%4.2f" % (event.preferred_magnitude.magnitude)
        else:
            mag = '----'

        return_str += "  <td>%s</td>\n" % mag
        return_str += "  <td>%s</td>\n" % event.selectflag
        return_str += "  <td>%s</td>\n" % event.version

        return_str += "</tr>\n"

        #continue

        name = "c-%d origins" % ii
        #cls = "assoc-%d_origins" % ii

        return_str += "<tr name='%s' class='hide' id='%s' style='visibility:collapse;'>\n" % (child_rid, child_rid)
        # MTH: The following cells are for to make sorttable.js work with the hidden origin-table
        for field in [event.evid, event.auth, event.subsource, '',
                      UTCDateTime(event.preferred_origin.datetime).strftime('%Y-%m-%dT%H:%M:%S.%f')[:-4],
                      event.preferred_origin.lat, event.preferred_origin.lon,
                      mag, event.selectflag, event.version]:
            return_str += "  <td style=\"display:none\">%s</td>\n" % field

        return_str += '''
<td colspan="100%">
<table class="origin-table">
    <thead>
      <tr>
        <th class="CellWithComment">orid<span class="CellComment">origin.orid</span></th>
        <th class="CellWithComment">auth<span class="CellComment">origin.auth</span></th>
        <th class="CellWithComment">subsrc<span class="CellComment">origin.subsource</span></th>
        <th class="CellWithComment">p<span class="CellComment">preferred origin</span></th>
        <th class="CellWithComment">locevid<span class="CellComment">origin.locevid</span></th>
        <th class="CellWithComment">origin time<span class="CellComment">origin time</span></th>
        <th class="CellWithComment">lat<span class="CellComment">origin latitude</span></th>
        <th class="CellWithComment">lon<span class="CellComment">origin longitude</span></th>
        <th class="CellWithComment">dep<span class="CellComment">origin depth</span></th>
        <th class="CellWithComment">narr<span class="CellComment">origin.totalarrs</span></th>
        <th class="CellWithComment">r<span class="CellComment">origin.rflag</span></th>
        <th>origin.lddate</th>
      </tr>
    </thead>
    <tbody>
'''
        for jj, origin in enumerate(event.origins):
            #print("jj=%d orid:%d" % (jj, origin.orid))
            preferred = "*" if origin.preferred else " "
            mag = None
            if getattr(origin, 'preferred_magnitude', None):
                mag = origin.preferred_magnitude
            elif origin.netmags:
                print("origin orid:%d has no preferred magnitude! --> use origin.netmags[0] magid:%d" %
                      (origin.orid, origin.netmags[0].magid))
                mag = origin.netmags[0]

            if origin.ndef:
                narr = "%3d" % origin.ndef
            # MTH: Some origins, eg EBird have totalarr=null
            elif getattr(origin, 'totalarr', None) and origin.totalarr > 0:
                narr = "%3d" % origin.totalarr
            else:
                narr = "---"

            clickmag = "%d-%s-netmags" % (jj, origin.orid)
            clickmag = "origin-%d netmags" % jj

            clickmag = "assoc-%d origin-%d netmags" % (ii, jj)

            if (jj % 2) == 0:
                return_str += "      <tr class='hover' name=\"origin-%d\" onclick=\"toggle_netmags('%s');\">\n" % (jj, clickmag)
            else:
                return_str += "      <tr class='hover even' name=\"origin-%d\" onclick=\"toggle_netmags('%s');\">\n" % (jj, clickmag)

            #return_str += "        <td>%d</td>\n" % origin.evid
            return_str += "        <td>%d</td>\n" % origin.orid
            return_str += "        <td>%s</td>\n" % origin.auth
            return_str += "        <td>%s</td>\n" % origin.subsource
            return_str += "        <td>%s</td>\n" % preferred
            return_str += "        <td>%s</td>\n" % origin.locevid
            return_str += "        <td>%s</td>\n" % UTCDateTime(origin.datetime).strftime('%Y-%m-%dT%H:%M:%S.%f')[:-4]
            return_str += "        <td>%7.3f</td>\n" % origin.lat
            return_str += "        <td>%8.3f</td>\n" % origin.lon
            return_str += "        <td>%5.1f</td>\n" % origin.depth
            return_str += "        <td>%s</td>\n" % narr
            return_str += "        <td>%s</td>\n" % origin.rflag
            return_str += "        <td>%s</td>\n" % UTCDateTime(origin.lddate).strftime('%Y-%m-%dT%H:%M:%S.%f')[:-4]


            mag_val = "%4.2f" % mag.magnitude if mag else "----"
            mag_type = "%s" % mag.magtype if mag else "-"
            #if getattr(mag, 'nobs', None):
            mag_nobs = "%2d" % mag.nobs if getattr(mag, 'nobs', None) else "--"

            mag_auth = "%s" % mag.auth if mag else "--"
            mag_rflag = "%s" % mag.rflag if mag else "-"
            mag_id = "%s" % mag.magid if mag else "-------"

            return_str += "<tr class='netmag' name=\"%s\" style=\"visibility:collapse;\">\n" % clickmag

            return_str += '''
<td colspan="100%">
  <table class="netmag-table">
    <thead>
      <tr>
        <th class="CellWithComment">magid<span class="CellComment">netmag.magid</span></th>
        <th class="CellWithComment">auth<span class="CellComment">netmag.auth</span></th>
        <th class="CellWithComment">subsrc<span class="CellComment">netmag.subsource</span></th>
        <th class="CellWithComment">p<span class="CellComment">orig.prefmag</span></th>
        <th class="CellWithComment">mag<span class="CellComment">netmag.magnitude</span></th>
        <th class="CellWithComment">T<span class="CellComment">netmag.magtype</span></th>
        <th class="CellWithComment">algo<span class="CellComment">netmag.magalgo</span></th>
        <th class="CellWithComment">nsta<span class="CellComment">netmag.nsta</span></th>
        <th class="CellWithComment">nobs<span class="CellComment">netmag.nobs</span></th>
        <th class="CellWithComment">r<span class="CellComment">netmag.rflag</span></th>
        <th class="CellWithComment">netmag.lddate<span class="CellComment">netmag.lddate</span></th>
      </tr>
    </thead>
    <tbody>
'''
            for netmag in origin.netmags:
                preferred = "*" if netmag.preferred else " "
                ld = UTCDateTime(netmag.lddate).strftime('%Y-%m-%dT%H:%M:%S.%f')[:-4]
                return_str += "<tr>\n"
                return_str += "  <td>%d</td>\n" % netmag.magid
                return_str += "  <td>%s</td>\n" % netmag.auth
                return_str += "  <td>%s</td>\n" % netmag.subsource
                #return_str += "  <td></td>\n"
                return_str += "  <td>%s</td>\n" % preferred
                return_str += "  <td>%4.2f</td>\n" % netmag.magnitude
                return_str += "  <td>%s</td>\n" % netmag.magtype
                return_str += "  <td>%s</td>\n" % netmag.magalgo
                return_str += "  <td>%s</td>\n" % netmag.nsta
                return_str += "  <td>%s</td>\n" % netmag.nobs
                return_str += "  <td>%s</td>\n" % netmag.rflag
                return_str += "  <td>%s</td>\n" % ld
                return_str += "</tr>\n"

            if not origin.netmags:
                null = 'N/A'
                return_str += "<tr>\n"
                return_str += "  <td>%d</td>\n" % origin.orid
                return_str += "  <td>%s</td>\n" % null
                return_str += "  <td>%s</td>\n" % null
                return_str += "  <td>%s</td>\n" % null
                return_str += "  <td>%s</td>\n" % null
                return_str += "  <td>%s</td>\n" % null
                return_str += "  <td>%s</td>\n" % null
                return_str += "  <td>%s</td>\n" % null
                return_str += "  <td>%s</td>\n" % null
                return_str += "</tr>\n"


            return_str += "   </tbody>\n"
            return_str += " </table>\n"
            return_str += " </td>\n"
            return_str += "</tr>\n"

        #return_str += "    <tr>"
        #return_str += '      <td colspan="100%%">rmk:%s</td>\n' % str(assoc.remark)
        #return_str += '      <td colspan="100%%">rmk:%s</td>\n' % "assoc.remark"
        #return_str += "    </tr>"

        return_str += "    </tbody>\n"
        return_str += "  </table>\n"
        return_str += " </td>\n"
        return_str += "</tr>\n"

    return_str += "</tbody>\n"
    return_str += "</table>\n"
    return_str += "<div class=\"center\"> <button name=\"action\" value=\"reload\">Reload Table</button></div>\n"

    return_str += "</form>\n"

    #return_str += add_javascript()
    return_str += "<script src=\"static/styles/v.js\"></script>\n"
    return_str += "</body>\n"
    return_str += "</html>\n"

    return return_str


def make_table(assocevents):
    return_str = html_head
    return_str += '''
<body>
<div class="topnav">
  <a class="active" href="/">AssocEvents</a>
  <a href="events">Events</a>
  <a href="eventprefmag">Eventprefmag</a>
  <a href="filters">Filters</a>
  <a href="magprefpriority">Magprefpriority</a>
</div>

<form id="assocEventsForm" name="assocEventsForm" method="post">
<!--
<form id="assocEventsForm" action="">
<form id="assocEventsForm" action="" method="post">
<button name="action" value="action" onclick="uncheckall();">Clear</button>
-->


<table class="event-table sortable">
  <thead>
    <tr>
      <th class="CellWithCommentBelow">evid<span class="CellCommentBelow">event id</span></th>
      <th class="CellWithCommentBelow">auth<span class="CellCommentBelow">event.auth</span></th>
      <th class="CellWithCommentBelow">subsrc<span class="CellCommentBelow">event.subsource</span></th>
      <th class="CellWithCommentBelow">origin time<span class="CellCommentBelow">origin time</span></th>
      <th class="CellWithCommentBelow">lat<span class="CellCommentBelow">origin latitude</span></th>
      <th class="CellWithCommentBelow">lon<span class="CellCommentBelow">origin longitude</span></th>
      <th class="CellWithCommentBelow">mag<span class="CellCommentBelow">event magnitude</span></th>
      <th class="CellWithCommentBelow" style=\"text-align: center; border-right: 2px solid black;\">mag.auth<span class="CellCommentBelow">netmag.auth</span></th>
      <th class="CellWithCommentBelow">evidassoc<span class="CellCommentBelow">associated evid</span></th>
      <th class="CellWithCommentBelow">auth<span class="CellCommentBelow">assocevent.auth</span></th>
      <th class="CellWithCommentBelow">subsrc<span class="CellCommentBelow">assocevent.subsource</span></th>
      <th>Remove</th>
    </tr>
  </thead>
  <tbody>
 '''

    background = 'light'
    for ii, assoc in enumerate(assocevents):
        event = assoc.event
        event_assoc = assoc.event_assoc
        name = "assoc-%s-%s" % (event.evid, event_assoc.evid)

        if ii > 0:
            #if assoc.event.evid != assocevents[ii-1].event.evid and \
               #assoc.event_assoc.evid != assocevents[ii-1].event_assoc.evid:
            if assoc.event.evid != assocevents[ii-1].event.evid:
                background = 'dark' if background == 'light' else 'light'

        rid = "assoc-%d" % ii
        child_rid = "%s_origins" % rid

        #if (ii % 2) == 0:
        if background == 'light':
            return_str += "<tr id='%s' class='hover' name='%s' onclick=\"toggle('%s');\">\n" % (rid, name, child_rid)
            #return_str += "<tr id='%s' class='hover' name='%s' onclick=\"toggle('%s');\">\n" % (clickname, name, clickname)
        else:
            return_str += "<tr id='%s' class='hover even' name='%s' onclick=\"toggle('%s');\">\n" % (rid, name, child_rid)
            #return_str += "<tr id='%s' class='hover even' name='%s' onclick=\"toggle('%s');\">\n" % (clickname, name, clickname)

        #return_str += "  <td>%d</td>\n" % event.evid
        return_str += "  <td><div style=\"font-weight:bold;\">%s</div></td>\n" % event.evid
        return_str += "  <td>%s</td>\n" % event.auth
        return_str += "  <td>%s</td>\n" % event.subsource
        return_str += "  <td>%s</td>\n" % UTCDateTime(event.preferred_origin.datetime).strftime('%Y-%m-%dT%H:%M:%S.%f')[:-4]
        # MTH lat
        return_str += "  <td>%7.3f</td>\n" % event.preferred_origin.lat
        return_str += "  <td>%8.3f</td>\n" % event.preferred_origin.lon

        origin = event.preferred_origin
        mag = None
        if getattr(event, 'preferred_magnitude', None):
            mag = event.preferred_magnitude
        elif getattr(origin, 'preferred_magnitude', None):
            mag = origin.preferred_magnitude
        elif origin.netmags:
            mag = origin.netmags[0]
        else:
            print("MTH: make_table: evid=%s orid=%s --> Has 0 Netmags!!" % (event.evid, origin.orid))

        # MTH: netmag.magnitude is non-nullable so either we have a mag or not
        if mag:
            mag_val = "%4.2f" % mag.magnitude
            mag_type = "%s" % mag.magtype
            mag_nobs = "%2d" % mag.nobs if getattr(mag, 'nobs', None) else "--"
            mag_auth = "%s" % mag.auth
            mag_rflag = "%s" % mag.rflag
            mag_id = "%s" % mag.magid
        else:
            mag_val = "----"
            mag_type = "-"
            mag_nobs = "--"
            mag_auth = "--"
            mag_rflag = "-"
            mag_id = "-------"


        return_str += "  <td>%s</td>\n" % mag_val
        #return_str += "  <td>%s</td>\n" % mag.auth
        #return_str += "  <td style=\"text-align: center; border-right: 2px solid black;\">%s</td>\n" % mag.auth
        return_str += "  <td style=\"text-align: center; border-right: 2px solid black;\">%s</td>\n" % mag_auth
        #return_str += "  <td><div class=\"border-right\">%s</div></td>\n" % event.auth
        return_str += "  <td><div style=\"font-weight:bold;\">%s</div></td>\n" % event_assoc.evid

        return_str += "  <td>%s</td>\n" % event_assoc.auth
        return_str += "  <td>%s</td>\n" % event_assoc.subsource

        row_id = "pair: (%s,%s)" % (event.evid, event_assoc.evid)
        return_str += "  <td>\n"
        return_str += "    <div class=\"fieldgroup\">\n"
        #return_str += "    <input type=\"checkbox\" name=\"%s,%s\" value=\"delete\" id=\"delete\">Remove Association\n" % \
                            #(event.evid, event_assoc.evid)
        #return_str += "    <input type=\"checkbox\" name=\"%s\" value=\"delete\" id=\"delete\">Remove Association\n" % row_id

        value = "(%s,%s)" % (event.evid, event_assoc.evid)
        _id = "(%s,%s)" % (event.evid, event_assoc.evid)
        name = "delete"
        return_str += "    <input type=\"checkbox\" name=\"%s\" value=\"%s\" id=\"%s\">Unassociate\n" % \
                            (name, value, _id)

        return_str += "    </div>\n"
        return_str += "  </td>\n"
        return_str += "</tr>\n"

        #continue

        name = "assoc-%d origins" % ii
        cls = "assoc-%d_origins" % ii
        return_str += "<tr name='%s' class='hide' id='%s' style='visibility:collapse;'>\n" % (name, cls)

        # MTH: The following cells are for to make sorttable.js work with the hidden origin-table
        for field in [event.evid, event.auth, event.subsource,
                      UTCDateTime(event.preferred_origin.datetime).strftime('%Y-%m-%dT%H:%M:%S.%f')[:-4],
                      event.preferred_origin.lat, event.preferred_origin.lon,
                      mag_val, mag_auth,
                      event_assoc.evid, event_assoc.auth, event_assoc.subsource]:
            return_str += "  <td style=\"display:none\">%s</td>\n" % field


        return_str += '''
<td colspan="100%">
<table class="origin-table">
    <thead>
      <tr>
        <th class="CellWithComment">evid<span class="CellComment">origin.evid</span></th>
        <th class="CellWithComment">orid<span class="CellComment">origin.orid</span></th>
        <th class="CellWithComment">prefor<span class="CellComment">event.prefor</span></th>
        <th class="CellWithComment">auth<span class="CellComment">origin.auth</span></th>
        <th class="CellWithComment">r<span class="CellComment">origin.rflag</span></th>
        <th class="CellWithComment">locevid<span class="CellComment">origin.locevid</span></th>
        <th class="CellWithComment">origin time<span class="CellComment">origin time</span></th>
        <th class="CellWithComment">lat<span class="CellComment">origin latitude</span></th>
        <th class="CellWithComment">lon<span class="CellComment">origin longitude</span></th>
        <th class="CellWithComment">dep<span class="CellComment">origin depth</span></th>
        <th class="CellWithComment">narr<span class="CellComment">origin.totalarr</span></th>
        <th class="CellWithComment">mag<span class="CellComment">magnitude value</span></th>
        <th class="CellWithComment">T<span class="CellComment">magnitude type</span></th>
        <th class="CellWithComment">nobs<span class="CellComment">number mag observations</span></th>
        <th class="CellWithComment">auth<span class="CellComment">netmag.auth</span></th>
        <th class="CellWithComment">r<span class="CellComment">netmag.rflag</span></th>
        <th class="CellWithComment">magid<span class="CellComment">netmag.magid</span></th>
      </tr>
    </thead>
    <tbody>
    <tbody style="background-color:#FFFFE0">
'''
        #<th class="CellWithComment" title="mag type">T<span class="CellComment">mag_type</span></th>
        #<th>mag</th>
        #<th>nobs</th>
        #<th>auth</th>
        #<th>r</th>
        origin1 = event.preferred_origin
        origin2 = event_assoc.preferred_origin
        known_auth = set([origin1.auth, origin2.auth])

        '''
        origins = [origin1, origin2]
        for origin in (event.origins + event_assoc.origins):
            if origin.auth not in known_auth:
                origins.append(origin)
        '''

        origins = event.origins + event_assoc.origins

        for jj, origin in enumerate(origins):
            # local_origin = origin from event (not event_assoc)
            # local_origin = True if origin.evid == event.evid else False
            local_origin = True if origin in event.origins else False

            preferred = "*" if origin.preferred else " "
            mag = None

            use_event = event
            if origin in event_assoc.origins:
                use_event = event_assoc
            if getattr(use_event, 'preferred_magnitude', None):
                mag = use_event.preferred_magnitude
            elif getattr(origin, 'preferred_magnitude', None):
                mag = origin.preferred_magnitude
            elif origin.netmags:
                print("origin orid:%d has no preferred magnitude! --> use origin.netmags[0] magid:%d" %
                      (origin.orid, origin.netmags[0].magid))
                mag = origin.netmags[0]

            if origin.ndef:
                narr = "%3d" % origin.ndef
            elif getattr(origin, 'totalarr', None) and origin.totalarr > 0:
                narr = "%3d" % origin.totalarr
            else:
                narr = "---"

            clickmag = "%d-%s-netmags" % (jj, origin.orid)
            clickmag = "origin-%d netmags" % jj

            clickmag = "assoc-%d origin-%d netmags" % (ii, jj)

            if (jj % 2) == 0:
                return_str += "      <tr class='hover' name=\"origin-%d\" onclick=\"toggle_netmags('%s');\">\n" % (jj, clickmag)
            else:
                return_str += "      <tr class='hover even' name=\"origin-%d\" onclick=\"toggle_netmags('%s');\">\n" % (jj, clickmag)

            return_str += "        <td>%d</td>\n" % origin.evid
            return_str += "        <td>%d</td>\n" % origin.orid
            return_str += "        <td style=\"text-align:center\">%s</td>\n" % preferred
            return_str += "        <td style=\"text-align:center\">%s</td>\n" % origin.auth
            return_str += "        <td style=\"text-align:center\">%s</td>\n" % origin.rflag
            return_str += "        <td style=\"text-align:center\">%s</td>\n" % origin.locevid
            return_str += "        <td style=\"text-align:center\">%s</td>\n" % UTCDateTime(origin.datetime).strftime('%Y-%m-%dT%H:%M:%S.%f')[:-4]
            return_str += "        <td style=\"text-align:center\">%7.3f</td>\n" % origin.lat
            return_str += "        <td style=\"text-align:center\">%8.3f</td>\n" % origin.lon
            return_str += "        <td style=\"text-align:center\">%5.1f</td>\n" % origin.depth
            return_str += "        <td>%s</td>\n" % narr

            mag_val = "%4.2f" % mag.magnitude if mag else "----"
            mag_type = "%s" % mag.magtype if mag else "-"
            #if getattr(mag, 'nobs', None):
            mag_nobs = "%2d" % mag.nobs if getattr(mag, 'nobs', None) else "--"

            mag_auth = "%s" % mag.auth if mag else "--"
            mag_rflag = "%s" % mag.rflag if mag else "-"
            mag_id = "%s" % mag.magid if mag else "-------"

            return_str += "        <td>%s</td>\n" % mag_val
            return_str += "        <td style=\"text-align:center\">%s</td>\n" % mag_type
            return_str += "        <td>%s</td>\n" % mag_nobs
            return_str += "        <td style=\"text-align:center\">%s</td>\n" % mag_auth
            return_str += "        <td style=\"text-align:center\">%s</td>\n" % mag_rflag
            return_str += "        <td>%s</td>\n" % mag_id
            return_str += "      </tr>\n"
            return_str += "<tr class='netmag' name=\"%s\" style=\"visibility:collapse;\">\n" % clickmag
            return_str += '''
            <td colspan="100%">
            <table class="netmag-table">
            <thead>
            <tr>
            <th></th>
            <th class="CellWithComment">orid<span class="CellComment">netmag.orid</span></th>
            <th class="CellWithComment">auth<span class="CellComment">netmag.auth</span></th>
            <th class="CellWithComment">mag<span class="CellComment">magnitude value</span></th>
            <th class="CellWithComment">T<span class="CellComment">magnitude type</span></th>
            <th class="CellWithComment">algo<span class="CellComment">magnitude algorithm</span></th>
            <th class="CellWithComment">magid<span class="CellComment">magnitude id</span></th>
            <th class="CellWithComment small">o.pmag<span class="CellComment">origin.prefmag</span></th>
            <th class="CellWithComment">r<span class="CellComment">netmag.rflag</span></th>
            <th class="CellWithComment">nobs<span class="CellComment">number mag observations</span></th>
            <th>netmag.lddate</th>
            </tr>
            </thead>
            <tbody>
            '''
            for netmag in origin.netmags:
                value = "(%s,%s)" % (event.evid, netmag.magid)
                _id = "(%s,%s)" % (event.evid, netmag.magid)
                netmag_from_assoc = False
                for orig in event_assoc.origins:
                    if orig.orid == netmag.orid:
                        netmag_from_assoc = True
                        break
                return_str += "<tr>\n"
                if local_origin:
                    #if netmag.external:
                    if netmag_from_assoc:
                        name = "delete_eventprefmag"
                        return_str += "  <td>\n"
                        return_str += "     <input type=\"checkbox\" name=\"%s\" value=\"%s\" id=\"%s\">Unselect Mag</input>\n" % \
                                (name, value, _id)
                        return_str += "  </td>\n"
                    else:
                        return_str += "  <td></td>\n"
                else:
                    name = "insert_eventprefmag"
                    return_str += "  <td>\n"
                    return_str += "     <input type=\"checkbox\" name=\"%s\" value=\"%s\" id=\"%s\">Select Mag</input>\n" % \
                            (name, value, _id)
                    return_str += "  </td>\n"

                preferred = "*" if netmag.preferred else " "
                ld = UTCDateTime(netmag.lddate).strftime('%Y-%m-%dT%H:%M:%S.%f')[:-4]

                return_str += "  <td>%d</td>\n" % netmag.orid
                # MTH:
                #return_str += "  <td>%d</td>\n" % origin.orid
                #return_str += "  <td class=\"center\">%s</td>\n" % netmag.auth
                return_str += "  <td>%s</td>\n" % netmag.auth
                return_str += "  <td>%4.2f</td>\n" % netmag.magnitude
                #return_str += "  <td style=\"text-align:center\">%s</td>\n" % netmag.magtype
                return_str += "  <td>%s</td>\n" % netmag.magtype
                return_str += "  <td>%s</td>\n" % netmag.magalgo
                return_str += "  <td>%d</td>\n" % netmag.magid
                return_str += "  <td>%s</td>\n" % preferred
                return_str += "  <td>%s</td>\n" % netmag.rflag
                return_str += "  <td>%s</td>\n" % netmag.nobs
                return_str += "  <td>%s</td>\n" % ld
                return_str += "</tr>\n"

            if not origin.netmags:
                null = 'N/A'
                return_str += "<tr>\n"
                return_str += "  <td></td>\n"
                return_str += "  <td>%d</td>\n" % origin.orid
                return_str += "  <td>%s</td>\n" % null
                return_str += "  <td>%s</td>\n" % null
                return_str += "  <td>%s</td>\n" % null
                return_str += "  <td>%s</td>\n" % null
                return_str += "  <td>%s</td>\n" % null
                return_str += "  <td>%s</td>\n" % null
                return_str += "  <td>%s</td>\n" % null
                return_str += "  <td>%s</td>\n" % null
                return_str += "</tr>\n"


            return_str += "   </tbody>\n"
            return_str += " </table>\n"
            return_str += " </td>\n"
            return_str += "</tr>\n"

        return_str += "    <tr>"
        #return_str += '      <td colspan="100%%">rmk:%s</td>\n' % str(assoc.remark)
        return_str += '      <td class="left" colspan=8>rmk:%s</td>\n' % str(assoc.remark)

        return_str += "      <td><div class=\"center\"><button type=\"button\" name=\"action\" value=\"apply\" onclick=\"applySelection()\">Apply Selection</button>"
        #return_str += "      <td><div class=\"center\"><button type=\"button\" name=\"action\" value=\"apply\" onclick=\"makeMagPreferred()\">Apply Selection</button>"

        #return_str += "      <button name=\"action\" value=\"submit-to-pdl %d\" name=\"%d\" id=\"%s\">Submit to PDL</button></div></td></div></td>\n" % \
        return_str += "      <button type=\"button\" name=\"action\" value=\"submit-to-pdl %d\" name=\"%d\" id=\"%s\" onclick=\"submitPDL(%d)\">Submit to PDL</button></div></td></div></td>\n" % \
                                (event.evid, event.evid, event.evid, event.evid)
        #return_str += "      <button name=\"action\" value=\"submit-to-pdl\" name=\"%d\" id=\"%s\">Submit to PDL</button></div></td></div></td>\n" % \
                                #(event.evid, event.evid)

        return_str += "    </tr>"

        return_str += "    </tbody>\n"
        return_str += "  </table>\n"
        return_str += " </td>\n"
        return_str += "</tr>\n"

    #clickname = "origs-%s-%s" % (event.evid, event_assoc.evid)
    clickname = "associate"
    #return_str += "<tr onclick=\"toggle('%s');\">\n" % (clickname)
    return_str += "<tr onclick=\"toggle_by_id('%s');\">\n" % ('+Associate')
    return_str += "  <td colspan=\"100%%\" style=\"color: blue;\">+Associate</td>\n"
    return_str += "  </td>\n"
    return_str += "</tr>\n"

    return_str += "<tr id='+Associate' name='%s' style='visibility:collapse;'>\n" % clickname
    #return_str += "<tr name='%s' style='visibility:collapse;'>\n" % "nosuchname"
    return_str += '''
<td colspan="100%">
  <table>
    <tr>
      <td colspan="100%">evid 1:<input type="text" id="evid1" name="evid1" size=10 maxlength=9 onkeypress="return onlyNumberKey(event)"/>
      evid 2:<input type="text" id="evid2" name="evid2" size=10 maxlength=9 onkeypress="return onlyNumberKey(event)"/>
      <button type="button" name="action" value="associate" onclick="myAssociate();">Associate</button>
      </td>
    <tr>
    <tr>
      <td colspan="100%">remark:<input type="text" id="rmk" name="rmk" size=50>
    </tr>
  </table>
</td>
</tr>
    '''

    return_str += "</tbody>\n"
    return_str += "</table>\n"
    return_str += "<div class=\"center\"> <button name=\"action\" value=\"reload\" id=\"reloadTable\">Reload Table</button></div>\n"

    return_str += "</form>\n"

    #return_str += add_javascript()
    return_str += "<script src=\"static/styles/v.js\"></script>\n"

    return_str += "</body>\n"
    return_str += "</html>\n"

    return return_str
