# -*- coding: utf-8 -*-
"""
GUI to view AQMS Assocevents and Event table and
make Mww magnitude preferred for local evid

:copyright:
    Mike Hagerty (m.hagerty@isti.com), 2022
:license:
    GNU Lesser General Public License, Version 3
    (https://www.gnu.org/copyleft/lesser.html)
"""

from flask import render_template, flash, redirect, url_for, request
from flask import session, jsonify
#from app.forms import LoginForm
from aqms_pdl_app.app import app
from aqms_pdl_app.app.forms import LoginForm
import decimal

logger = app.logger

import os

from aqms_pdl_app.aqms_db import update_html, update_events, query_magprefpriority
from aqms_pdl_app.aqms_db import query_eventprefmag

from aqms_pdl.libs.libs_db import delete_assocevent, insert_assocevent, get_eventbyid
from aqms_pdl.libs.libs_db import call_procedure

# In order to use the libs_db funcs from aqms-pdl, 
#   we need to configure the Session in that module
from aqms_pdl.libs.libs_db import set_Session
from aqms_pdl.libs.libs_db import set_eventprefmag, unset_eventprefmag
from aqms_pdl import createSession
config = {'sqlalchemy.url': app.config['SQLALCHEMY_DATABASE_URI']}
#print("app/routes.py: createSession(config)")
createSession(config)
# import the configured Session into the aqms_pdl.libs.libs_db pkg for use
set_Session()


#def submit_to_pdl(evid):
@app.route('/submit-to-pdl', methods=['GET', 'POST'])
def submit_to_pdl():
    """
    p_group = "TPP"
    p_source = "TPP"
    p_id = evid
    p_state = "FINALIZE"
    p_rank = 100
    """
    form = request.get_json()
    evid = form['evid']

    if 'SUBMIT_PDL' in app.config:
        params = {}
        for key in ['p_group', 'p_source', 'p_state', 'p_rank']:
            params[key] = app.config['SUBMIT_PDL'][key.upper()]

        #print("call pcs.post_id(p_group=%s, p_source=%s, evid=%s p_state=%s, p_rank=%s)" %
                              #(params['p_group'], params['p_source'], evid,
                               #params['p_state'], params['p_rank']))
        logger.info("call pcs.post_id(p_group=%s, p_source=%s, evid=%s p_state=%s, p_rank=%s)" %
                              (params['p_group'], params['p_source'], evid,
                               params['p_state'], params['p_rank']))

        retval = call_procedure('pcs.post_id',
                                [params['p_group'], params['p_source'], evid,
                                params['p_state'], params['p_rank']],
                                config)

        msg = "submit_to_pdl(evid=%d) returned retval=%s" % (evid, retval)
        logger.info(msg)
    else:
        msg = "submit_to_PDL: Unable to submit to PDL since app.config['SUBMIT_PDL'] params NOT SET !!"
        logger.warning(msg)
        retval = -9

    return jsonify({'retval':retval, 'retmsg':msg})


@app.route('/make_mag_preferred', methods=['GET', 'POST'])
def make_mag_preferred():
    form = request.get_json()
    evid = form['evid']
    magid = form['magid']
    use_force = False
    if 'force' in form:
        use_force = form['force']
        #print("flask: Got force:%s" % use_force)
    #print("flask: call set_eventprefmag(evid:%d, magid:%d)" % (evid, magid))
    logger.info("call set_eventprefmag(evid:%d, magid:%d)" % (evid, magid))
    retdict = set_eventprefmag(evid, magid, config, force=use_force)
    logger.info("set_eventprefmag: return retdict:%s" % retdict)
    return jsonify(retdict)

@app.route('/make_mag_unpreferred', methods=['GET', 'POST'])
def make_mag_unpreferred():
    form = request.get_json()
    evid = form['evid']
    magid = form['magid']
    logger.info("call unset_eventprefmag(evid:%d)" % evid)
    retdict = unset_eventprefmag(evid, config, ignore_existing_prefmag=False)
    logger.info("unset_eventprefmag: return retdict:%s" % retdict)
    return jsonify(retdict)

@app.route('/unassociate', methods=['GET', 'POST'])
def unassociate():
    form = request.get_json()
    evid = form['evid']
    evidassoc = form['evidassoc']
    logger.info("call delete_assocevent(evid:%d, evidassoc:%d)" % (evid, evidassoc))
    delete_assocevent(evid, evidassoc)
    return jsonify({'retval':200, 'retmsg':'Success'})

@app.route('/associate', methods=['GET', 'POST'])
def associate():
    form = request.get_json()
    evid1 = form['evid1']
    evid2 = form['evid2']
    rmk = form['rmk'] if form['rmk'] else None

    try:
        evid1 = int(form['evid1'])
        evid2 = int(form['evid2'])
    except ValueError as e:
        retmsg = "associate: Unable to parse ints from evid1:%s and/or evid2:%s" % (form['evid1'], form['evid2'])
        logger.error(retmsg)
        return jsonify({'retval':-1, 'retmsg':retmsg})

    event1 = get_eventbyid(evid1)
    event2 = get_eventbyid(evid2)

    if event1 is None:
        retmsg = "associate: evid1=%s is not a valid evid in db!" % (evid1)
        logger.warning(retmsg)
        return jsonify({'retval':-1, 'retmsg':retmsg})
    elif event2 is None:
        retmsg = "associate: evid2=%s is not a valid evid in db!" % (evid2)
        logger.warning(retmsg)
        return jsonify({'retval':-1, 'retmsg':retmsg})

    retdict = insert_assocevent(evid1, evid2, rmk)
    logger.info("associate: insert_assocevent returned:%s" % retdict)
    return jsonify(retdict)


@app.route('/', methods=['GET', 'POST'])
def index():

    if 'invalid' in session and session['invalid']:
        return redirect(url_for('filters'))

    if 'user' in session:
        logger.info("Found session['user']=[%s]" % session['user'])
    else:
        # Initialize session for new user
        import string
        import random
        #letters = string.ascii_lowercase  ascii_uppercase ascii_digits, etc
        letters = string.ascii_letters
        session['user'] = ''.join(random.choice(letters) for i in range(12))
        logger.info("Set session['user']=[%s]" % session['user'])
        for key in ['update-html', 'update-events', 'update-magpref',
                    'update-eventprefmag']:
            session[key] = True
        session['limit_maxrows'] = 'checked'
        session['max_rows'] = app.config['MAXROWS']
        sdir = os.path.join(app.root_path, 'static/static_files/', session['user'])
        import pathlib
        pathlib.Path(sdir).mkdir(parents=True, exist_ok=True)

    #print("Enter index method:%s form:%s" % (request.method, request.form))

    if request.method == "POST":
        form = request.form
        #if form['action'] == 'apply':
            #if 'delete' in form.keys():
                #key = form['delete']
                #eval turns string "1,2,3" into (1,2,3) tuple
                #delete_assocevent(*eval(key))
            #elif 'insert_eventprefmag' in form.keys():
                #key = form['insert_eventprefmag']
                #ret = set_eventprefmag(*eval(key), config)

        session['update-html'] = True
        session['update-events'] = True
        session['update-eventprefmag'] = True
        return redirect(url_for('index'))

    html_file = 'static_files/' + session['user'] + '/assocevents.html'
    disk_file = os.path.join(app.root_path, 'static', html_file)

    if session['update-html']:
        update_html(session, disk_file)
        session['update-html'] = False

    return app.send_static_file(html_file)



@app.route('/events', methods=['GET', 'POST'])
def events():

    if 'invalid' in session and session['invalid']:
        return redirect(url_for('filters'))

    html_file = 'static_files/' + session['user'] + '/events.html'
    disk_file = os.path.join(app.root_path, 'static', html_file)

    #print("/events: request.method=%s session['limit_maxrows']=%s" % (request.method, session['limit_maxrows']))

    if request.method == "POST" or session['update-events']:
        form = request.form
        #if form['action'] == 'reload':
        #print("call update_events session['limit_maxrows']=%s" % session['limit_maxrows'])
        update_events(session, disk_file)
        session['update-events'] = False
        redirect(url_for('events'))

    return app.send_static_file(html_file)
    # This works:
    #return app.send_static_file('events.html')
    # This does NOT:
    #return app.send_static_file('/events.html')


@app.route('/eventprefmag', methods=['GET', 'POST'])
def eventprefmag():
    '''
     Column  |            Type             | Collation | Nullable |                 Default
---------+-----------------------------+-----------+----------+------------------------------------------
 evid    | bigint                      |           | not null |
 magtype | character varying(6)        |           | not null |
 magid   | bigint                      |           | not null |
 lddate  | timestamp without time zone |           |          | timezone('UTC'::text, CURRENT_TIMESTAMP)
    '''

    if 'invalid' in session and session['invalid']:
        return redirect(url_for('filters'))

    # Probably don't need to make a separate table for every user
    html_file = 'static_files/' + session['user'] + '/eventprefmag.html'
    disk_file = os.path.join(app.root_path, 'static', html_file)

    if request.method == "POST" or session['update-eventprefmag']:
        form = request.form

        #print("Call query_eventprefmag")
        #print(session)
        rows = query_eventprefmag(session)
        #print("query_eventprefmag returned [%d] rows" % len(rows))
        mdicts = []
        for eventprefmag, event, netmag in rows:
            mdicts.append(
                {'evid': eventprefmag.evid,
                 'event_auth': event.auth,
                 'magid': eventprefmag.magid,
                 'mag': netmag.magnitude,
                 'magtype': eventprefmag.magtype,
                 'mag_auth': netmag.auth,
                 'lddate': eventprefmag.lddate,
                })
        #mm = query_eventprefmag()
        # Turn list of table rows into list of dicts
        #mdicts = [m.__dict__ for m in mm]
        #for k,v in mdicts[0].items():
            #print("k=%s --> v=%s" % (k,v))
        for m in mdicts:
            for k,v in m.items():
                if isinstance(v, decimal.Decimal):
                    m[k] = float(v)

        html = render_template('eventprefmag.html', query_rows=mdicts)

        with open(disk_file, 'w') as f:
            f.write(html)

        session['update-eventprefmag'] = False

        return redirect(url_for('eventprefmag'))


    #print("Return eventprefmag static file")
    return app.send_static_file(html_file)



@app.route('/magprefpriority', methods=['GET', 'POST'])
def magprefpriority():
    """
    region_name| datetime_on| datetime_off | prior|magt| auth | subsource |    magalgo     | minmag | maxmag |minrd|maxuncer| quality
-----------+------------+--------------+------+----+------+-----------+----------------+--------+--------+-----+--------+--------
 DEFAULT  | -2209000000 |  32504000000 |   99 | w  | US   |           | Mww            |      3 |     10 |     |        |
 DEFAULT  | -2209000000 |  32504000000 |   90 | w  | NC   |           | Mw             |      5 |     10 |     |        |
 DEFAULT  | -2209000000 |  32504000000 |   85 | w  | NC   |           | TMTSUI         |      5 |     10 |     |   0.75 |
 DEFAULT  | -2209000000 |  32504000000 |   75 | w  | NC   |           | TMTSUI         |      3 |     10 |     |    0.5 |
 DEFAULT  | -2209000000 |  32504000000 |   70 | w  | NC   |           | TMTS           |    3.5 |     10 |     |    0.5 |
 DEFAULT  | -2209000000 |  32504000000 |   65 | w  | NC   |           | TMTSUI         |     -3 |      3 |     |    0.5 |
 DEFAULT  | -2209000000 |  32504000000 |   55 | l  | NC   | Jiggle    | CISNml2        |      3 |     10 |     |        |
    """

    if 'invalid' in session and session['invalid']:
        return redirect(url_for('filters'))

    # Probably don't need to make a separate table for every user
    #html_file = 'static_files/' + session['user'] + '/events.html'
    html_file = 'static_files/magpref.html'
    disk_file = os.path.join(app.root_path, 'static', html_file)

    if request.method == "POST" or session['update-magpref']:
        form = request.form

        #for k,v in form.items():
            #print("k=%s --> v=%s" % (k,v))

        #if form['magpref'] == 'reload':
            #print("** Magpref reload requested")

        #print("Call query_magprefpriority")
        mm = query_magprefpriority()
        # Turn list of magprefpriority class into list of dicts as below
        mdicts = [m.__dict__ for m in mm]
        #for k,v in mdicts[0].items():
            #print("k=%s --> v=%s" % (k,v))
        for m in mdicts:
            for k,v in m.items():
                if isinstance(v, decimal.Decimal):
                    m[k] = float(v)

        html = render_template('magpref.html', magprefrows=mdicts)

        #print("Write magpref table to disk_file=[%s]" % disk_file)
        with open(disk_file, 'w') as f:
            f.write(html)

        session['update-magpref'] = False
        return html
        #return render_template('magpref.html', magprefrows=mdicts)

    else:
        #print("Return static_file=[%s]" % html_file)
        return app.send_static_file(html_file)



@app.route('/filters', methods=['GET', 'POST'])
def filters():

    if request.method == 'GET':
        #print("Got GET on filters")
        # Initialize session keys if this is first time
        for key in ['date_checked', 'evid_checked', 'mag_checked', 'limit_maxrows']:
            if key not in session:
                session[key] = ''
        for key in ['min_date', 'max_date', 'min_evid', 'max_evid',
                    'min_mag', 'max_mag', 'max_rows']:
            if key not in session:
                session[key] = None

    elif request.method == 'POST':
        #print("Got POST on filters, action=%s" % request.form['action'])
        clear = True if 'action' in request.form and request.form['action'] == 'clear' else False

        if not clear:
            session['invalid'] = check_form()
            #print("POST check_form returned invalid=%s" % invalid)
            #if errors:
                #for error in errors:
                    #flash(error)

        modify_session()
        #request.method = 'POST' # This doesn't work
        # code=307 means redirect using same method as requesting, so POST-->POST,
        # which is necessary when filter.clear_all is selected, so /events reloads table
        return redirect('/events', code=307)

    return render_template('filters.html', session=session)

def modify_session():

    clear = True if 'action' in request.form and request.form['action'] == 'clear' else False
    #print("modify_session: clear=%s" % clear)

    if clear:
        session['errors'] = []
        for key in ['date_errors', 'evid_errors', 'mag_errors', 'limit_maxrows']:
            session[key] = ''
            if key == 'limit_maxrows':
                logger.info("modify_session: clear session['limit_maxrows']")
                #print("modify_session: clear session['limit_maxrows']")
        session['invalid'] = False

    for key in ['date_checked', 'evid_checked', 'mag_checked', 'limit_maxrows']:

        if clear or key not in request.form:
            session[key] = ''
        else:
            session[key] = 'checked'
        #print("modify_session: set session[%s] = %s" % (key, session[key]))

    for key in ['min_date', 'max_date', 'min_evid', 'max_evid',
                'min_mag', 'max_mag', 'max_rows']:
        if clear or key not in request.form:
            session[key] = None
        else:
            session[key] = request.form[key]
        #print("modify_session: set session[%s] = %s" % (key, session[key]))

    # 2023-06-14 MTH: it seems like any time we pass through modify_session, we will want
    #                 to update *all* the tables: assocevents, events, eventprefmag
    # when /events calls redirect(/events, code=307) above, /events will see POST and
    # will reload events, but what about the other tables ?
    # Seems like I should always reload them when clearing the filters

    # Flag the update keys so next click on these tables triggers reload
    #if not clear:
        #for key in ['update-events', 'update-html', 'update-eventprefmag']:
            #session[key] = True

    for key in ['update-events', 'update-html', 'update-eventprefmag']:
        session[key] = True

    return


def check_form():

    invalid = False
    errors = []

    session['errors'] = []
    for key in ['date_errors', 'evid_errors', 'mag_errors', 'row_errors']:
        session[key] = ''

    if 'limit_maxrows' in request.form:
        max_rows = request.form['max_rows']
        if not max_rows:
            session['row_errors'] = '*Please enter an integer > 0'
            invalid = True

    if 'date_checked' in request.form:
        min_date = request.form['min_date']
        max_date = request.form['max_date']
        #print("check_form: min_date=[%s] max_date=[%s]" % (min_date, max_date))
        if not min_date:
            errors.append("Please enter a valid min_date")
            session['date_errors'] = '*Please enter a valid min_date'
            invalid = True
        if not max_date:
            errors.append("Please enter a valid max_date")
            invalid = True
            if min_date:
                session['date_errors'] = '*Please enter a valid max_date'
        if min_date and max_date and min_date > max_date:
            errors.append("Please enter a max_date > min_date")
            session['date_errors'] = '*Please enter a max_date > min_date'
            invalid = True

    if 'evid_checked' in request.form:
        min_evid = int(request.form['min_evid']) if request.form['min_evid'] else None
        max_evid = int(request.form['max_evid']) if request.form['max_evid'] else None
        if not min_evid:
            errors.append("Please enter a valid min_evid")
            invalid = True
            session['evid_errors'] = '*Please enter a valid min_evid'
        if not max_evid:
            errors.append("Please enter a valid max_evid")
            invalid = True
            if min_evid:
                session['evid_errors'] = '*Please enter a valid max evid'
        if min_evid and max_evid and min_evid > max_evid:
            errors.append("*Please enter a max_evid > min_evid")
            invalid = True
            session['evid_errors'] = '*Please enter a max_evid > min_evid'

    if 'mag_checked' in request.form:
        min_mag = float(request.form['min_mag']) if request.form['min_mag'] else None
        max_mag = float(request.form['max_mag']) if request.form['min_mag'] else None
        if not min_mag:
            errors.append("Please enter a valid min_mag")
            invalid = True
            session['mag_errors'] = '*Please enter a valid min_mag'
        if not max_mag:
            errors.append("Please enter a valid max_mag")
            invalid = True
            if min_mag:
                session['mag_errors'] = '*Please enter a valid max_mag'
        if min_mag and max_mag and min_mag > max_mag:
            errors.append("Please enter a max_mag > min_mag")
            invalid = True
            session['mag_errors'] = '*Please enter a max_mag > min_mag'

    session['errors'] = errors

    return invalid

'''
We need to pass a form object to the login.html template.
'''
@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        flash('Login requested for user {}, remember_me={}'.format(
            form.username.data, form.remember_me.data))
        #return redirect('/index')
        return redirect(url_for('index'))
    return render_template('login.html', title='Sign In', form=form)
