
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from . import dbconn_string as SQLALCHEMY_DATABASE_URL

#engine = create_engine(SQLALCHEMY_DATABASE_URL, echo=True, connect_args={"check_same_thread": False})
#print("database.py: create_engine with SQLALCHEMY_DATABASE_URL=%s" % SQLALCHEMY_DATABASE_URL)
engine = create_engine(SQLALCHEMY_DATABASE_URL, echo=False)
Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
session = Session()
