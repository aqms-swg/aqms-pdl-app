
import os

from flask import Flask
from aqms_pdl_app.config import Config
#from flask_sqlalchemy import SQLAlchemy
from aqms_pdl_app.app.flask_logs import LogSetup
from aqms_pdl.libs.libs_log import read_config

from obspy import UTCDateTime

'''
https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-i-hello-world-legacy
If you are wondering why the import statement is at the end and not
at the beginning of the script as it is always done, the reason is
to avoid circular references, because you are going to see that the
views module needs to import the app variable defined in this script.
Putting the import at the end avoids the circular import error.
'''

app = Flask(__name__)

#app = Flask(__name__,
            #static_url_path='',
            #static_folder='/static',
            #static_folder='app/static',
            #template_folder='web/templates'
            #)

#app.config.from_object(Config)

# In case this is being called in the old way,
#   e.g, >flask run -h 0.0.0.0 -p 5001
# See if we can read a config to set up Config
# To use app.config we have to uncomment app.config.from_object(Config)
#   above, but I'd rather do it once after Config it totally set
#if 'SQLALCHEMY_DATABASE_URI' not in app.config:
if getattr(Config, 'SQLALCHEMY_DATABASE_URI', None) is None:
    print("Starting from start.sh --> Try to read_config")
    config, log_msgs = read_config(requireConfigFile=True, debug=False)

    for msg in log_msgs['info']:
        print("Info:%s" % msg)

    if config is None:
        for msg in log_msgs['error']:
            print("Error:%s" % msg)
        exit(2)

    # DEFAULTS - Note that only capitalized params get saved in app.config
    #            Lowercase params will get dropped
    fname = 'aqms-pdl-app'
    params = {}
    params['HOST'] = 'localhost'
    params['PORT'] = 5000
    params['MAXDAYS'] = 7
    params['MAXROWS'] = 14
    params['LOG_DIR'] = 'log'
    params['LOG_LEVEL'] = 'INFO'
    params['APP_NAME'] = '%s' % fname
    params['APP_LOG_NAME'] = '%s.log' % fname
    params['WWW_LOG_NAME'] = "localhost:5000"
    params['SEND_FILE_MAX_AGE_DEFAULT'] = 0
    params['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    params['SQLALCHEMY_DATABASE_URI'] = None

    app_fields =  ['host', 'port', 'MAXDAYS', 'MAXROWS', 'APP_LOG_NAME',
                  'LOG_LEVEL', 'LOG_DIR',]
    for field in app_fields:
        if 'flask' in config and field in config['flask']:
            params[field] = config['flask'][field]

    import sys
    for i, arg in enumerate(sys.argv):
        if arg in ['-h', '--host']:
            params['HOST'] = sys.argv[i+1]
        elif arg in ['-p', '--port']:
            params['PORT'] = sys.argv[i+1]

    params['SQLALCHEMY_DATABASE_URI'] = config['sqlalchemy.url']

    Config.set_args(**params)
    #app.config.from_object(Config)
else:
    # Config was already set by flask_aqms.py
    pass

# temp fix until I decide what's needed:
Config.EVENTS = {}
Config.ASSOCEVENTS = {}
Config.ASSOCEVENTS['MAXROWS'] = Config.MAXROWS
#Config.ASSOCEVENTS['MAXDAYS'] = params['MAXDAYS']
Config.EVENTS['MAXROWS'] = Config.MAXROWS
Config.EVENTS['MAXDAYS'] = 5000

# Now set app.config:
app.config.from_object(Config)

dbconn_string = app.config['SQLALCHEMY_DATABASE_URI']

# And configure app.logger
logs = LogSetup()
logs.init_app(app)

width = 30
now = UTCDateTime.utcnow().strftime('%Y-%m-%dT%H:%M:%S.%f')[:-4]

logger = app.logger

txt="[ Start of %s ]" % Config.APP_NAME
logger.info(f"{txt: >{width}}".format(txt=txt, width=width))
logger.info(f"{'utc' : >{width}}:{now}".format(width=width))
logger.info(f"{'host': >{width}}:{os.uname().nodename}".format(width=width))
logger.info(f"{'--host': >{width}}:{Config.HOST}".format(width=width))
logger.info(f"{'port': >{width}}:{Config.PORT}".format(width=width))

for field in ['MAXDAYS', 'MAXROWS', 'LOG_DIR', 'APP_LOG_NAME', 'LOG_LEVEL',
              'SEND_FILE_MAX_AGE_DEFAULT', 'SQLALCHEMY_TRACK_MODIFICATIONS',
              'SQLALCHEMY_DATABASE_URI',]:
              #'SQLALCHEMY_DATABASE_URI', 'ENV', 'DEBUG']:

    if field == 'SQLALCHEMY_DATABASE_URI':
        # obscure user + pw in log:
        #postgresql://user:xxx-pw-xxx@localhost:5432/myarch
        user = app.config[field].split(':')[1].replace('//','')
        pw = app.config[field].split(':')[2].split('@')[0]
        logger.info(f"{field:>{width}}:{app.config[field].replace(pw, 'xxxxx').replace(user, 'user')}".format(width=width))
    else:
        logger.info(f"{field:>{width}}:{app.config[field]}".format(width=width))

#field = "BRANCH"
#value = "* set-unset-prefmag *"
#logger.info(f"{field:>{width}}:{value}".format(width=width))

#db = SQLAlchemy(app)

#print("MTH: __init__: __name__=%s" % __name__)
#from app import views
#from app import routes, models

app.config['SECRET_KEY'] = os.urandom(24).hex()

from aqms_pdl_app.app import routes
