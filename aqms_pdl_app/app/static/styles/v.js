
var URL_BASE = window.location.origin;
console.log(`JS URL_BASE=${URL_BASE}`);


async function submitPDL(evid)
{
  alert(`submitPDL evid:${evid}`);

  let _settings = {'evid': evid}

  let route =  '/submit-to-pdl';
  let url = URL_BASE + route;
  let data = null;
  let response

  try {
      response = await fetch(url, {
        method: "POST",
        body: JSON.stringify(_settings),
        headers: {
          "Content-type": "application/json; charset=UTF-8"
        }
      });
      //data = await response.json();
      //console.log(data);
   } catch(err) {
      console.error(err);
      alert(`caught error:${err}`);
   }
   data = await response.json();
   console.log(`route: ${route} returned data:`);
   console.log(data);

   if (data['retval'] == 0){
     msg = "submit to PDL was successful!";
     alert(msg);
     document.getElementById('reloadTable').click();
   }
   else {
    msg = `submit-to-pdl(${evid}) returned:\n\n`;
    msg += `retval:${data['retval']}  retmsg:${data['retmsg']}`;
    alert(msg);
   }
}

//function applySelection() {
async function applySelection() {
    let form = document.getElementById("assocEventsForm");
    let formData = new FormData(form);

    for (let [key, value] of formData.entries()) {
      console.log(`formData[${key}] = ${value}`);
      //_settings[key]=value
    }

    let action = null;
    let route = null;
    let evid = null;
    let magid = null;

  /* Handle: ApplySelection {Unassociate, SetPrefMag, UnsetPrefMag} */

    routes = {'insert_eventprefmag': '/make_mag_preferred',
              'delete_eventprefmag': '/make_mag_unpreferred',
              'delete': '/unassociate',
             }

    for (const [_action, _route] of Object.entries(routes)) {
      console.log(_action, _route);
      const arg = formData.get(_action);
      if (arg){
        const fields = arg.split(',');
        evid  = parseInt(fields[0].replace("(", ''));
        magid = parseInt(fields[1].replace(")", ''));
        action= _action;
        route = _route;
        if (action == "delete"){
          evidassoc = magid;  // 2nd fields in this case is actually evidassoc, not magid
          console.log(`action:${action} evid:${evid} evidassoc:${evidassoc} route:${route}`);
        }
        else {
          console.log(`action:${action} evid:${evid} magid:${magid} route:${route}`);
        }
        break;
      }
    }

    if (action == null){
      alert('Please select an action first, e.g., Select/Unselect Mag or Unassociate');
      //document.getElementById('reloadTable').click();
      return
    }

    if (action == 'insert_eventprefmag') {
      //alert('redirect to makeMagPreferred');
      return makeMagPreferred(evid, magid);
    }

    let _settings = {'evid': evid}
    if (action == 'delete') {
      _settings['evidassoc'] = evidassoc;
    }
    else {
      _settings['magid'] = magid;
    }

    let url = URL_BASE + route;
    let data = null;
    let response
    console.log(`action:${action} _settings:${_settings}`);

    try {
      response = await fetch(url, {
        method: "POST",
        body: JSON.stringify(_settings),
        headers: {
          "Content-type": "application/json; charset=UTF-8"
        }
      });
    } catch(err) {
      console.error(err);
      alert(`caught error:${err}`);
    }
    data = await response.json();
    console.log(`route: ${route} returned data:`);
    console.log(data);

    if (action == 'delete') {
      alert("Unassociate was successful!");
      document.getElementById('reloadTable').click();
      return
    }
    else if (action == 'delete_eventprefmag') {
      if (data['retval'] == 1) {
        alert("Unsetprefmag was successful!");
        document.getElementById('reloadTable').click();
        return
      } else {
        alert(`Unsetprefmag failed: retval:${data['retval']} retmsg:${data['retmsg']}`);
      }
    }

  }


async function makeMagPreferred(evid, magid){

    route = '/make_mag_preferred';
    let _settings = {'evid':evid, 'magid':magid}
    _settings['force'] = false;

    let url = URL_BASE + route;
    let data = null;

    let response
    try {
      response = await fetch(url, {
        method: "POST",
        body: JSON.stringify(_settings),
        headers: {
          "Content-type": "application/json; charset=UTF-8"
        }
      });
    } catch(err) {
      console.error(err);
      alert("caught error");
      alert(err);
    }
    data = await response.json();
    console.log(data);

    if (data['retmsg']) {

      const allowForce = true;
      if (allowForce) {
        const yes = confirm(`Unable to set prefmag using stored procedure.\
        \n\n${data['retmsg']}.\
        \n\nDo you want to *force* set prefmag ?`);

        if (yes) {
          try {
            _settings['force'] = true;
            const responseX = await fetch(url, {
              method: "POST",
              body: JSON.stringify(_settings),
              headers: {
                "Content-type": "application/json; charset=UTF-8"
              }
            });
            data = await responseX.json();
            console.log(data);
          } catch(err) {
            console.error(err);
            alert(err);
          }
          if (data['retmsg']){
            alert(`Still Unable to set prefmag!\n\n${data["retmsg"]}`);
          }
        } // yes
      } // allowForce
      else {
        msg = `Unable to make magid:${magid} preferred on evid:${evid}\n`;
        alert(msg + data['retmsg']);
      }
    } // insert_eventprefmag

    if (data['retmsg']) {
      //alert("Setprefmag failed!");
    }
    else {
        alert("Setprefmag was successful!");
        document.getElementById('reloadTable').click();
      //window.location.href = URL_BASE;
    }

  }


async function myAssociate() {
  var evid1 = document.getElementById('evid1').value;
  var evid2 = document.getElementById('evid2').value;
  var rmk = document.getElementById('rmk').value;
  //alert(`evid1=${evid1} evid2=${evid2}`);
  if (!evid1){
      alert('evid1 is NOT set');
      return
  }
  if (!evid2){
      alert('evid2 is NOT set');
      return
  }

  let url = URL_BASE + '/associate';
  let data = null;

  //alert("Call flask /associate");
  let response
  const _settings = {'evid1':evid1, 'evid2':evid2, 'rmk':rmk};
  try {
    response = await fetch(url, {
      method: "POST",
      body: JSON.stringify(_settings),
      headers: {
        "Content-type": "application/json; charset=UTF-8"
      }
    });

  } catch(err) {
    console.error(err);
    alert(`caught error:${err}`);
  }
  data = await response.json();
  console.log(data);
  if (data['retval'] == 200) {
    //window.location.href = URL_BASE;
    alert('Insert into assocevents was successful!');
    document.getElementById('reloadTable').click();
  }
  else if (data['retmsg']){
    msg = `Unable to associate evid1:${evid1} and evid2:${evid2}\n\n`;
    alert(msg + data['retmsg']);
  }
}

function onlyNumberKey(evt) {

    // Only ASCII character in that range allowed
    var ASCIICode = (evt.which) ? evt.which : evt.keyCode
    if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
        return false;
    return true;
}

function toggle_by_id(id)
{
    var elem = document.getElementById(id);
    if (elem.style.visibility === "collapse") {
        elem.style.visibility = "visible";
    } else {
        elem.style.visibility = "collapse";
    }
}

function toggle(assoc_id)
{
    var x = document.getElementById(assoc_id);
    //alert('function toggle assoc_id='+assoc_id+' Got x=' + x);
    //var id = assoc_id + '_origins';
    var id = assoc_id;
    var elem = document.getElementById(id);

    //var elem = x.querySelectorAll("tr.origin")[0];
    //var elem = document.querySelectorAll("tr.origin")[0];

    var netmags = elem.querySelectorAll("tr.netmag");

    if (elem.style.visibility === "collapse") {
        elem.style.visibility = "visible";
    } else {
        for(var i = 0; i < netmags.length; i++) {
            netmags[i].style.visibility = "collapse";
        }
        elem.style.visibility = "collapse";
    }
}

function toggle_netmags(name)
{
    var elem = document.getElementsByName(name)[0];
    if (elem.style.visibility === "collapse") {
        elem.style.visibility = "visible";
    } else {
        elem.style.visibility = "collapse";
    }
}

function uncheckall(){
    document.querySelectorAll('input[type=checkbox]').forEach(el => el.checked = false);
}


