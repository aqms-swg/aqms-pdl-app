# -*- coding: utf-8 -*-
"""
GUI to view AQMS Assocevents and Event table and
make Mww magnitude preferred for local evid

:copyright:
    Mike Hagerty (m.hagerty@isti.com), 2022
:license:
    GNU Lesser General Public License, Version 3
    (https://www.gnu.org/copyleft/lesser.html)
"""

from obspy import UTCDateTime
from datetime import datetime
from sqlalchemy.exc import SQLAlchemyError

#import logging
#logger = logging.getLogger('app')

# Let the app read in connect info from config.py and pass it into here
from aqms_pdl_app.app import app

MAXDAYS = app.config['MAXDAYS']
MAXROWS = app.config['MAXROWS']

logger = app.logger

# MTH: If you do this you have to figure out how to
#      get Session.session to do queries as they exist
#from aqms_pdl import Session as session

# MTH: Need to distinguish sqlalchemy session from flask session dict!
from aqms_pdl_app.app.database import session as db_session
from aqms_pdl_app.app.database import Session

from aqms_pdl.libs.libs_log import read_config
from aqms_pdl.libs.schema_aqms_PI import Origin, Assocevents, Remark, Event, Netmag
from aqms_pdl.libs.schema_aqms_PI import Eventprefmag, Magprefpriority

from aqms_pdl.libs.libs_db import get_leapsecs

from aqms_pdl_app.aqms_tables import make_table, make_events_table

# MTH: Not using anything in libs_db
# Configure the Session for the whole module in __init__.py:
#from aqms_pdl.libs.libs_db import set_Session
#from aqms_pdl import createSession
#config = {'sqlalchemy.url': app.config['SQLALCHEMY_DATABASE_URI']}
#createSession(config)
# import the configured Session into the aqms_pdl.libs.libs_db pkg for use
#set_Session()

def clean_session_args(session):
    kwargs = {}
    kwargs['filter_maxrows'] = False
    kwargs['filter_date'] = False
    kwargs['filter_evid'] = False
    kwargs['filter_mag']  = False
    if 'limit_maxrows' in session and session['limit_maxrows']=='checked':
        logger.debug("update_events: set max_rows=%s" % session['max_rows'])
        kwargs['filter_maxrows'] = True
        kwargs['max_rows'] = int(session['max_rows'])
    if 'date_checked' in session and session['date_checked']=='checked':
        kwargs['filter_date'] = True
        kwargs['min_datetime'] = UTCDateTime(session['min_date']).timestamp
        # Add 24 hours to include events on max_date
        kwargs['max_datetime'] = (UTCDateTime(session['max_date']) + 86399.99).timestamp
        #kwargs['max_datetime'] = UTCDateTime(session['max_date']).timestamp

    if 'evid_checked' in session and session['evid_checked']=='checked':
        kwargs['filter_evid'] = True
        kwargs['min_evid'] = int(session['min_evid'])
        kwargs['max_evid'] = int(session['max_evid'])
    if 'mag_checked' in session and session['mag_checked']=='checked':
        kwargs['filter_mag'] = True
        kwargs['min_mag'] = float(session['min_mag'])
        kwargs['max_mag'] = float(session['max_mag'])

    return kwargs


"""
def update_html(session, html_file):
    print("Enter update_html()")
    #assocevents = query_assocevents_all(maxrows=app.config['ASSOCEVENTS']['MAXROWS'])
    assocevents = query_assocevents_all(maxrows=int(session['max_rows']))

    html = make_table(assocevents)
    #print("update_html --> save to ./html_file")
    with open(html_file, 'w') as f:
        f.write(html)
    return html
"""

def update_html(session, html_file):

    kwargs = clean_session_args(session)
    assocevents = query_assocevents(kwargs)

    html = make_table(assocevents)

    with open(html_file, 'w') as f:
        f.write(html)
    return html


def update_events(session, html_file):

    #min_mag = app.config['EVENTS']['MIN_MAG'] if 'MIN_MAG' in app.config['EVENTS'] else None
    #events = query_events(maxdays=app.config['EVENTS']['MAXDAYS'], maxrows=app.config['EVENTS']['MAXROWS'],
                          #min_mag=min_mag)

    #print("update_events session:")
    #print(session)
    #print()

    kwargs = clean_session_args(session)
    events = query_events(kwargs)

    if events is None:
        logger.error("ERROR: update_events: query_events returned None!")
    else:
        logger.info("update_events: %d sorted events" % len(events))
    html = make_events_table(events)

    with open(html_file, 'w') as f:
        f.write(html)
    return html


def get_query(filter_date=False, filter_evid=False, filter_mag=False, filter_maxrows=False,
                 min_datetime=None, max_datetime=None,
                 min_evid=None, max_evid=None,
                 min_mag=None, max_mag=None,
                 max_rows=None,
                 ):

    fname='get_query'

    #print("get_evids: filter_date=%s filter_maxrows=%s max_rows=%s" % (filter_date, filter_maxrows, max_rows))

    with Session.begin() as session:

        query = session.query(Event.evid).join(Origin, Origin.orid==Event.prefor).\
                                          filter(Event.etype=='eq').\
                                          filter((Event.selectflag==1) |
                                                 (Event.subsource=='PDL')).\
                                          order_by(Origin.datetime.desc())
        if filter_date:
            logger.debug("%s: Filter dates: %s -to- %s" % (fname, min_datetime, max_datetime))
            query = query.filter(Origin.datetime >= min_datetime).\
                          filter(Origin.datetime <= max_datetime)

        if filter_mag:
            logger.debug("%s: Filter %.2f <= mag <= %.2f" % (fname, min_mag, max_mag))
            query = query.join(Netmag, Netmag.magid==Event.prefmag).\
                        filter(Netmag.magnitude >= min_mag).\
                        filter(Netmag.magnitude <= max_mag).\
                        order_by(Netmag.magnitude.desc())

        if filter_evid:
            logger.debug("%s: Filter evids: %d -to- %d" % (fname, min_evid, max_evid))
            query = query.join(Netmag, Netmag.magid==Event.prefmag, isouter=True).\
                        filter(Event.evid >= min_evid).\
                        filter(Event.evid <= max_evid).\
                        order_by(Event.evid.desc())

        session.expunge_all()

    #if filter_maxrows:
        #logger.debug("%s: Filter max_rows:%d" % (fname, max_rows))
        #query = query.limit(max_rows)

    #evids = query.all()
    #evids = [tup[0] for tup in evids]

    return query


def query_events(kwargs):

    fname='query_events'

    logger.info("%s: filter_date=%s filter_mag=%s filter_evid=%s filter_maxrows=%s" %
                (fname, kwargs['filter_date'], kwargs['filter_mag'], kwargs['filter_evid'], kwargs['filter_maxrows']))

    filter_maxrows = True if 'filter_maxrows' in kwargs and kwargs['filter_maxrows'] else False
    filter_others = False
    for k in ['filter_date', 'filter_evid', 'filter_mag']:
        if kwargs[k] == True:
            filter_others = True
            break

    if not filter_maxrows and not filter_others:
        logger.warning("%s: No filters set --> query entire Event table!" % fname)

    query = get_query(**kwargs)
    if filter_maxrows:
        query = query.limit(kwargs['max_rows'])

    evids = query.all()
    evids = [tup[0] for tup in evids]

    #for i, evid in enumerate(evids):
        #print("[%2d] evid:%s" % (i, evid))

    events = []
    origins = []

    with Session.begin() as session:
        query = session.query(Event, Origin).filter(Event.evid==Origin.evid).\
                                                filter(Event.evid.in_(evids)).\
                                                order_by(Origin.datetime.desc())
        results = query.all()
        session.expunge_all()

    if len(results) == 0:
        logger.info("query_events returned 0 results!")
        for k,v in kwargs.items():
            logger.debug("query_events param:%s value:%s" % (k,v))
        logger.warning("%s returned 0 results!" % fname)
        return []

    #print("query returned %d results" % (len(results)))
    #for event,origin in results:
        #print("results evid:%d orig.evid:%d orid:%d" % (event.evid, origin.evid, origin.orid))

    for event, origin in results:
        origins.append(origin)
        events.append(event)

    #for i, event in enumerate(events):
        #print("[%2d] evid:%d" % (i, event.evid))

    #logger.info("%s: query with start_date=%s end_date=%s start_evid=%s end_evid=%s min_mag=%s max_mag=%s returned [%d] rows" %
                #(fname, start_date, end_date, start_evid, end_evid, min_mag, max_mag, len(results)))


    # The maxrows cutoff may cut off a prefor origin that we need
    orids = [origin.orid for origin in origins]
    preforids = [event.prefor for event in events]
    missing = [event.prefor for event in events if event.prefor not in orids]
    #for orid in missing:
        #origin = db_session.query(Origin).filter(Origin.orid==orid).one_or_none()
        #origins.append(origin)
        #logger.warning("Cut off: evid:%d prefor:%d is missing --> get it now" %
                       #(origin.evid, orid))

    with Session.begin() as session:
        missing_origins = session.query(Origin).filter(Origin.orid.in_(missing)).all()
        logger.warning("%s: Picked up [%d] missing/cut-off origins" % (fname, len(missing_origins)))
        origins.extend(missing_origins)
        session.expunge_all()

    origins = sorted(origins, key=lambda x: x.orid, reverse=True)
    orid_max = origins[0].orid
    orid_min = origins[-1].orid

    #for orig in origins:
        #print(orig.evid, orig.orid)

    orids = [origin.orid for origin in origins]

    with Session.begin() as session:
        #netmags = session.query(Netmag).filter(Netmag.orid >= orid_min).filter(Netmag.orid <= orid_max).order_by(Netmag.orid.desc()).all()
        netmags = session.query(Netmag).filter(Netmag.orid.in_(orids)).order_by(Netmag.orid.desc()).all()
        session.expunge_all()
    #print("Netmags:")
    #for netmag in netmags:
        #print(netmag.orid, netmag.magid)

    remove_leapsecs = True if get_leapsecs(UTCDateTime.now().timestamp) else False

    if remove_leapsecs:
        for i, origin in enumerate(origins):
            leapsecs = get_leapsecs(origin.datetime)
            #print("i:%d remove [%d] leapsecs from orid:%d before:%f after:%f" %
                 #(i, int(leapsecs), origin.orid, origin.datetime, origin.datetime-leapsecs))
            origin.datetime -= leapsecs

    events = set(events)

    datetimes = []
    for event in events:
        event.origins = [origin for origin in origins if origin.evid == event.evid]
        add_netmags_to_event_origins(event, netmags)
        datetimes.append(event.preferred_origin.datetime)

    # Even though we ordered the sql query, when we created a set from the list of events,
    #    it loses order (as of python 3.8)
    # Sort events with most recent first
    sorted_events = [x for _, x in sorted(zip(datetimes,events), key=lambda pair: pair[0], reverse=True)]

    if 0:
        for event in sorted_events:
            print("evid:%d" % event.evid)
            for origin in event.origins:
                print("  orid:%d" % origin.orid)
                for netmag in origin.netmags:
                    print("    magid:%d" % netmag.magid)

    logger.info("%s: return sorted_events" % fname)
    return sorted_events


def add_netmags_to_event_origins(event, netmags):
    '''
        event should contain event.origins specific to this evid
        Similar to add_origins_and_netmags_to_event() but faster
           since does one big query for origins & netmags before it gets here
    '''

    evid = event.evid

    # Loop over origin(s) for this event and attach netmag(s) to each
    #   Look to make origin preferred on the event
    #   Look to make netmag preferred on the origin
    for origin in event.origins:
        origin.preferred = False
        if getattr(event, 'prefor', None):
            if origin.orid == event.prefor:
                origin.preferred = True
                event.preferred_origin = origin
        origin.netmags = [netmag for netmag in netmags if netmag.orid == origin.orid]
        if len(origin.netmags) == 0:
            #print("origin.orid=%d origin.evid=%d --> Has No netmags!" %
                  #(origin.orid, origin.evid))
            logger.info("origin.orid=%d origin.evid=%d --> Has No netmags!" %
                  (origin.orid, origin.evid))
        for netmag in origin.netmags:
            #print("orid:%d has netmag:%d" % (origin.orid, netmag.magid))
            netmag.preferred = False
            if netmag.magid == origin.prefmag:
                netmag.preferred = True
                origin.preferred_magnitude = netmag
                if origin.preferred and event.prefmag != netmag.magid:
                    logger.warning("evid:%d event.prefmag=%d but event.prefor=%d and event.preferred_origin.prefmag=%d" %
                                    (event.evid, event.prefmag, event.prefor, origin.prefmag))

    netmag_pref = None

    # Set netmag_pref for the event
    # 1. event.prefmag
    if getattr(event, 'prefmag', None):
        logger.debug("evid:%d has prefmag:%d Get prefmag from event.prefmag" % (evid, event.prefmag))
        for netmag in netmags:
            if netmag.magid == event.prefmag:
                netmag_pref = netmag
                break
    elif getattr(event, 'prefor', None):
        # 2. event.prefor.prefmag
        if getattr(event.preferred_origin, 'prefmag', None):
            logger.debug("evid:%d has prefor:%d which has orig.prefmag:%d Get prefmag from prefor.prefmag" % 
                        (evid, event.prefor, event.preferred_origin.prefmag))
            #print("evid:%d has prefor:%d which has orig.prefmag:%d Get prefmag from prefor.prefmag" % 
                        #(evid, event.prefor, event.preferred_origin.prefmag))
            netmag_pref = event.preferred_origin.preferred_magnitude

        # 3. event.prefor.netmags[0]
        else:
            if len(event.preferred_origin.netmags) == 1:
                netmag_pref = event.preferred_origin.netmags[0]
                logger.debug("evid:%d has prefor:%d which 1 netmag magid:%d --> make this preferred" %
                            (evid, event.prefor, netmag_pref.magid))
                #print("evid:%d has prefor:%d which 1 netmag magid:%d --> make this preferred" %
                            #(evid, event.prefor, netmag_pref.magid))
            else:
                logger.warning("evid:%d has prefor:%d which has %d netmags --> No way to set prefmag!" %
                                (evid, event.prefor, len(event.preferred_origin.netmags)))
    else:
        logger.warning("evid:%d does not have prefmag -or- prefor set --> No way to determine prefmag!" % evid)


    if netmag_pref:
        netmag_pref.preferred = True
        event.preferred_magnitude = netmag_pref

        # Add PDL netmag to local pref origin
        #if netmag_pref not in event.preferred_origin.netmags:
        if netmag_pref not in event.preferred_origin.netmags and netmag_pref.auth != event.preferred_origin.auth:
            event.preferred_origin.netmags.append(netmag_pref)
            logger.warning("evid:%d prefmag magid:%d belongs to a foreign origin --> Add pref netmag to this preforigin" %
                          (evid, netmag_pref.magid))
            #print("evid:%d prefmag magid:%d belongs to a foreign origin --> Add pref netmag to this preforigin" %
                          #(evid, netmag_pref.magid))
    return


def add_origins_and_netmags_to_event(event):
    '''
    Given an aqms event object, attach all affiliated origins 
       to it, and all affiliated netmags to the origins
    Make the sql queries here
    '''
    evid = event.evid
    netmag_pref = None

    import sqlalchemy

    with Session.begin() as session:
    # Decide on a single netmag_pref for the event
    # 1. event.prefmag
        if getattr(event, 'prefmag', None):
            logger.debug("evid:%d has prefmag:%d Get prefmag from event.prefmag" % (evid, event.prefmag))
            try:
                netmag_pref = session.query(Netmag).filter_by(magid=event.prefmag).one()
            except sqlalchemy.exc.NoResultFound:
                logger.warning("evid:%d prefmag:%d --> No such row in Netmag table!!" % (evid, event.prefmag))
                netmag_pref = None

        elif getattr(event, 'prefor', None):
            origin = session.query(Origin).filter_by(orid=event.prefor).one()
            # 2. event.prefor.prefmag
            if getattr(origin, 'prefmag', None):
                logger.debug("evid:%d has prefor:%d which has orig.prefmag:%d Get prefmag from prefor.prefmag" % 
                            (evid, event.prefor, origin.prefmag))
                netmag_pref = session.query(Netmag).filter_by(magid=origin.prefmag).one()
            # 3. event.prefor.netmags[0]
            else:
                netmags = session.query(Netmag).filter_by(orid=event.prefor).all()
                logger.debug("evid:%d has prefor:%d which %d netmag rows" % 
                            (evid, event.prefor, len(netmags)))
                if len(netmags) == 1:
                    netmag_pref = netmags[0]
                elif len(netmags) == 0:
                    logger.warning("evid:%d has prefor:%d but no netmag rows --> No way to set event prefmag" % 
                                    (evid, event.prefor))
        else:
            logger.warning("evid:%d No Way to FIND prefmag!" % (evid))

        if netmag_pref:
            netmag_pref.preferred = True
            event.preferred_magnitude = netmag_pref

        #logger.info("MTH evid:%d has prefmag:%s prefor:%d" % (evid, event.prefmag, event.prefor)) 
        #logger.debug("evid:%d Got prefmag_id:%d" % (evid, netmag_pref.magid))

        # Loop over origins or this event.
        #   For each origin attach associated netmags to this origin
        origins = []
        for origin in session.query(Origin).filter_by(evid=evid).order_by(Origin.lddate.desc()).all():
            origin.preferred = False
            if origin.orid == event.prefor:
                origin.preferred = True
                event.preferred_origin = origin
                #datetimes.append(origin.datetime)
            origin.netmags = session.query(Netmag).filter_by(orid=origin.orid).order_by(Netmag.lddate.desc()).all()
            if len(origin.netmags) == 0:
                logger.warning("Evid:%d orid:%d --> Found 0 netmags!" % (evid, origin.orid))
            for netmag in origin.netmags:
                netmag.preferred = False
                netmag.external = False
                if netmag.magid == origin.prefmag:
                    netmag.preferred = True
                    origin.preferred_magnitude = netmag
                    if origin.preferred and event.prefmag != netmag.magid:
                        logger.warning("evid:%s event.prefmag=%s but event.prefor=%s and event.preferred_origin.prefmag=%s" %
                                        (event.evid, event.prefmag, event.prefor, origin.prefmag))

            origins.append(origin)

        remove_leapsecs = True if get_leapsecs(UTCDateTime.now().timestamp) else False
        if remove_leapsecs:
            for i, origin in enumerate(origins):
                leapsecs = get_leapsecs(origin.datetime)
                #print("i:%d remove [%d] leapsecs from orid:%d before:%f after:%f" %
                    #(i, int(leapsecs), origin.orid, origin.datetime, origin.datetime-leapsecs))
                origin.datetime -= leapsecs

        event.origins = origins

        session.expunge_all()

    if netmag_pref:
        netmag_pref.external = False
    # For local events that have event.prefmag set to a PDL netmag,
    #    the PDL netmag won't be in the local event.origins[] and must be added
        found = False
        for origin in event.origins:
            for netmag in origin.netmags:
                if netmag.magid == netmag_pref.magid:
                    found = True
                    break
        if not found:
            netmag_pref.external = True
            event.preferred_origin.netmags.append(netmag_pref)
            logger.info("evid:%d prefmag magid:%d belongs to a foreign origin orid:%d --> Add pref netmag to this preforigin" %
                    (evid, netmag_pref.magid, netmag_pref.orid))

    #print("evid:%s prefmag:%d external:%s" % (evid, netmag_pref.magid, netmag_pref.external))

    return



#def query_assocevents_all(maxrows=400):
def query_assocevents(kwargs):
    '''
        query Assocevents table, return list of EvtAssoc ordered by origin time descending
    '''
    fname = 'query_assocevents'

    class EvtAssoc():
        def __init__(self, event, event_assoc, remark=None):
            self.event = event
            self.event_assoc = event_assoc
            self.remark = remark

    filter_maxrows = True if 'filter_maxrows' in kwargs and kwargs['filter_maxrows'] else False
    filter_others = False
    for k in ['filter_date', 'filter_evid', 'filter_mag']:
        if kwargs[k] == True:
            filter_others = True
            break

    if not filter_maxrows and not filter_others:
        logger.warning("%s: No filters set --> query entire Assocevents table!" % fname)

    if filter_others:

        #evid_query = get_query(**kwargs)
        #evids = evid_query.all()
        evids = get_query(**kwargs).all()
        evids = [tup[0] for tup in evids]
        logger.debug("%s: get_query returned %d evids" % (fname, len(evids)))

    with Session.begin() as session:
        query = session.query(Assocevents, Remark).join(Event, Event.evid == Assocevents.evid).\
                                                   join(Origin, Origin.orid == Event.prefor, isouter=True).\
                                                   join(Remark, Remark.commid == Assocevents.commid, isouter=True).\
                                                   order_by(Origin.lddate.desc())

        if filter_others:
            query = query.filter(Assocevents.evid.in_(evids))

        if filter_maxrows:
            logger.debug("%s: limit query max_rows=%d" % (fname, kwargs['max_rows']))
            query = query.limit(kwargs['max_rows'])

        results = query.all()
        session.expunge_all()

    #print("After limit max_rows=%d ==> [%d] rows left" % (kwargs['max_rows'], len(results)))
    #for assoc, rmk in results:
        #print(assoc.evid, assoc.evidassoc)

    #logger.info("%s: query with maxrows=%d returned [%d] rows" % (fname, maxrows, len(results)))
    #print("%s: query with maxrows=%d returned [%d] rows" % (fname, maxrows, len(results)))

    datetimes = []
    event_associations = []

    for assoc, remark in results:
        evid = assoc.evid
        evidassoc = assoc.evidassoc
        #print("Assemble evid:%d evidassoc:%d" % (evid, evidassoc))
        event       = db_session.query(Event).get(evid)
        event_assoc = db_session.query(Event).get(evidassoc)

        #print("  add_origins_and_netmags evid:%d" % (evid))
        add_origins_and_netmags_to_event(event)
        #print("  add_origins_and_netmags evidassoc:%d" % (evidassoc))
        add_origins_and_netmags_to_event(event_assoc)

        if event.preferred_origin:
            datetimes.append(event.preferred_origin.datetime)

        if remark:
            event_associations.append(EvtAssoc(event, event_assoc, remark=remark.remark))
        else:
            event_associations.append(EvtAssoc(event, event_assoc, remark='None'))

    # Sort event associations by preferred origin times with most recent first
    #ordered_assocs = [x for _, x in sorted(zip(datetimes,event_associations), key=lambda pair: pair[0], reverse=False)]
    ordered_assocs = [x for _, x in sorted(zip(datetimes,event_associations), key=lambda pair: pair[0], reverse=True)]

    return ordered_assocs


def query_magprefpriority():

    fname='query_magprefpriority'

    rows = db_session.query(Magprefpriority).order_by(Magprefpriority.priority.desc()).all()

    return rows

def query_eventprefmag(session):
    fname='query_eventprefmag'
    #print(session)
    kwargs = clean_session_args(session)
    #print(kwargs)
    return get_query_eventprefmag(**kwargs)

def get_query_eventprefmag(filter_date=False, filter_evid=False, filter_mag=False, filter_maxrows=False,
                           min_datetime=None, max_datetime=None,
                           min_evid=None, max_evid=None,
                           min_mag=None, max_mag=None,
                           max_rows=None,
                           ):

    fname='get_query_eventprefmag'

    logger.info("%s: filter_date=%s filter_mag=%s filter_evid=%s filter_maxrows=%s maxrows=%s" %
                (fname, filter_date, filter_mag, filter_evid, filter_maxrows, max_rows))

    if not filter_maxrows and not filter_date and not filter_mag and not filter_evid:
        logger.warning("%s: No filters set --> query entire Eventprefmag table!" % fname)

    #query = db_session.query(Eventprefmag, Event, Netmag).filter(Eventprefmag.evid==Event.evid).\

    with Session.begin() as session:
        query = session.query(Eventprefmag, Event, Netmag).filter(Eventprefmag.evid==Event.evid).\
                                                              filter(Eventprefmag.magid==Netmag.magid).\
                                                              order_by(Eventprefmag.evid.desc())
        if filter_date:
            logger.debug("%s: Filter dates: %s -to- %s" % (fname, min_datetime, max_datetime))
            query = query.join(Origin, Origin.evid==Event.evid).filter(Origin.datetime >= min_datetime).\
                                                                filter(Origin.datetime <= max_datetime)

        if filter_mag:
            logger.debug("%s: Filter %.2f <= mag <= %.2f" % (fname, min_mag, max_mag))
            query = query.join(Netmag, Netmag.magid==Event.prefmag).\
                        filter(Netmag.magnitude >= min_mag).\
                        filter(Netmag.magnitude <= max_mag).\
                        order_by(Netmag.magnitude.desc())

        if filter_evid:
            logger.debug("%s: Filter evids: %d -to- %d" % (fname, min_evid, max_evid))
            query = query.join(Netmag, Netmag.magid==Event.prefmag, isouter=True).\
                        filter(Event.evid >= min_evid).\
                        filter(Event.evid <= max_evid).\
                        order_by(Event.evid.desc())

        if filter_maxrows:
            query = query.limit(max_rows)

        rows = query.all()

        session.expunge_all()

    logger.info("%s: return [%d] Eventprefmag rows" % (fname, len(rows)))

    return rows


