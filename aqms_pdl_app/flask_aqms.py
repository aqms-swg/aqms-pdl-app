# -*- coding: utf-8 -*-
"""
GUI to view AQMS Assocevents and Event table and
make Mww magnitude preferred for local evid

:copyright:
    Mike Hagerty (m.hagerty@isti.com), 2022
:license:
    GNU Lesser General Public License, Version 3
    (https://www.gnu.org/copyleft/lesser.html)
"""

import logging
import os
import sys
from obspy import UTCDateTime

from aqms_pdl.libs.libs_log import configure_logger, read_config
from aqms_pdl.libs.cmd_line import parse_cmd_line, required_arg, optional_arg

fname = 'aqms-pdl-app'

def main():
    """
    AQMS PDL App - Flask app for interacting with AQMS tables

    >>>aqms-pdl-app --configfile /path/to/config.yml >> log.txt 2>&1 &
    """

    o = []
    o.append(optional_arg(arg=['--host'], type=str, description='ipaddr',
                     help='host ipaddr'))
    o.append(optional_arg(arg=['-p', '--port'], type=int, description='port',
                     help='host port'))
    o.append(optional_arg(arg=['--maxrows'], type=int, description='maxrows',
                     help='maxrows to display'))
    o.append(optional_arg(arg=['--maxdays'], type=int, description='maxdays',
                     help='maxdays to display'))

    # We will automatically get --logfile, --logdir, --loglevel, --configfile
    args, parser = parse_cmd_line(optional_args=o)
    #parser.description = description

    config, log_msgs = read_config(configfile=args.configfile, debug=False)

    # local logger is only used here to log error and exit
    # app logging will be handled by flask logger
    if config is None:
        configure_logger(**{'logconsole': True})
        logger = logging.getLogger()
        for msg in log_msgs['info']:
            logger.info(msg)
        for msg in log_msgs['error']:
            logger.error(msg)
        print()
        parser.print_help()
        exit(2)

    params = {}
    # DEFAULTS - Note that only capitalized params get saved in app.config
    #            Lowercase params will get dropped
    params['HOST'] = '0.0.0.0'
    params['PORT'] = 5000
    params['MAXDAYS'] = 7
    params['MAXROWS'] = 14
    params['LOG_DIR'] = 'log'
    params['LOG_LEVEL'] = 'INFO'
    params['APP_NAME'] = '%s' % fname
    params['APP_LOG_NAME'] = '%s.log' % fname
    params['WWW_LOG_NAME'] = "localhost:5000"
    params['SEND_FILE_MAX_AGE_DEFAULT'] = 0
    params['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    params['SQLALCHEMY_DATABASE_URI'] = None

    app_fields =  ['HOST', 'PORT', 'MAXDAYS', 'MAXROWS', 'APP_LOG_NAME',
                   'APP_NAME', 'LOG_LEVEL', 'LOG_DIR',]
    # Default flask params (above) will be preferentially be overwritten
    #   by cmd line args, and failing that, from config file
    # For LOG_LEVEL, look for args.LOG_LEVEL, args.log_level, args.loglevel, etc
    for field in app_fields:
        if getattr(args, field, None):
            params[field] = getattr(args, field)
        elif getattr(args, field.lower(), None):
            params[field] = getattr(args, field.lower())
        elif getattr(args, field.lower().replace('_',''), None):
            params[field] = getattr(args, field.lower().replace('_',''))

        elif 'flask' in config:
            if field in config['flask']:
                params[field] = config['flask'][field]
            elif field.lower() in config['flask']:
                params[field] = config['flask'][field.lower()]

    params['SQLALCHEMY_DATABASE_URI'] = config['sqlalchemy.url']
    #if 'sqlalchemy.url' in config:
        #config['SQLALCHEMY_DATABASE_URI'] = config['sqlalchemy.url']

    # MTH: Currently app either logs to file -OR- to console (if logconsole flagged)
    # ATODO: modify app/flask_logs.py to enable both file/console logging
    if args.logconsole:
        params['LOG_TYPE'] = "stream"

    if 'SUBMIT_PDL' in config:
        params['SUBMIT_PDL'] = config['SUBMIT_PDL']
        #print("set params['SUBMIT_PDL'] from config")

    from aqms_pdl_app.config import Config
    Config.set_args(**params)

    # Import app here, after Config so that
    #   when app imports Config it is populated
    from aqms_pdl_app.app import app

    # MTH: this probably does nothing!
    app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0
    app.run(host=params['HOST'], port=params['PORT'])

    return


if __name__ == '__main__':
    main()
