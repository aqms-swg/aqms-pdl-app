# aqms-pdl-app

aqms-pdl-app is a flask app for interacting with AQMS tables, starting
with assocevents, event and eventprefmag tables.


## Installation

### Requirements

This app heavily relies on the aqms-pdl pkg (e.g., that pkg controls all
of the AQMS db interaction).

### Installing aqms-pdl-app

The app needs to find a yaml config file that
tells it how to connect to the AQMS db.  The app itself can be started
from any directory.

#### Quick Install

    >conda activate aqms   // or cd to your virtual env, etc
    (aqms)>pip install git+https://oauth2:glpat-ka_DQWRUG43ZANxUq1Np@gitlab.com/aqms-swg/aqms-pdl-app.git
    (aqms)>aqms-pdl-app --configfile ../aqms-configs/config.loc --port 5001 --loglevel DEBUG >/dev/null 2>&1 &

Point browser to:
http://localhost:5001

#### Usage

    >aqms-pdl-app -h

    usage: aqms-pdl-app [-h] [--configfile path/to/config.yml] [--loglevel DEBUG, INFO, WARN, ERROR] [--logconsole]
                    [--logfile /path/to/logfile] [--logdir /path/to/logfile] [--host ipaddr] [-p port] [--maxrows maxrows]
                    [--maxdays maxdays]

    optional arguments:
    -h, --help            show this help message and exit

    optional arguments:
    --configfile path/to/config.yml, --configFile path/to/config.yml, --config-file path/to/config.yml, --config_file path/to/config.yml
    --loglevel DEBUG, INFO, WARN, ERROR, --log-level DEBUG, INFO, WARN, ERROR
    --logconsole
    --logfile /path/to/logfile
    --logdir /path/to/logfile, --log-dir /path/to/logfile
    --host ipaddr         host ipaddr
    -p port, --port port  host port
    --maxrows maxrows     maxrows to display
    --maxdays maxdays     maxdays to display

#### The configuration file

The config file is often named "config.yml" but you can call it whatever
you want.  Here's an example:

```
# DB params are mandatory, however, if DB_PASSWORD is missing,
# it will look for it in the DB_PASSWORD env var
DB_HOST: localhost
DB_NAME: myarch
DB_PORT: 5432
DB_USER: rtem
DB_PASSWORD: xxxx_xxxx

# LOG params are optional, these are the defaults
LOG_DIR: logs
LOG_LEVEL: INFO

# These params are only used when starting the app from
#   python (method 2. below)
flask:
  host: 0.0.0.0
  port: 5005

# These params can be overriden on cmd line
#   and can be changed in the Filters tab
MAXROWS: 55
MAXDAYS: 555
```

The app will look for the
name of the config file in the following order:

1. from the command line (--configfile ...)

2. from the AQMS_DB_CONFIG env var  // e.g., export AQMS_DB_CONFIG=... before starting the app

3. from the local file "./config.yml" if it exists


#### Using the flask app

##### A note on browsers

  As of this writing (01/2022), this app has been tested on the latest
   Chrome + Microsoft Edge browsers.
  It does **not** play well with Safari - For some reason Safari
   *still* does not support dropdown table rows in css.
  Please let me know if you find a work around.

##### [09/2022] Navigation Bar added to GUI
The app has recently been modified to include a navigation bar at the top and links for
several table views (Assocevents, Events, Eventprefmag, Magprefpriority) 
and a tab for Filters to control filtering of the tables has been added.

Note that within any tab (=table view), you can click on the column headers and the table
will re-sort by that field.  Click twice to toggle between ascending/descending.

![new_GUI](docs/12.png)

##### [09/2022] Submit-to-PDL button added

![new_GUI](docs/14.png)

If you add the following fields in your config file:

    SUBMIT_PDL:
        P_GROUP: TPP
        P_SOURCE: TPP
        P_STATE: FINALIZE
        P_RANK: 100

Then the Submit PDL button will work and will inject the correct tuple
into the stored procedure pcs.post_id().
My understanding is that most RSN's won't want to set the state to FINALIZE since
that will re-alarm the event, but will instead create a new pcs_transition to
do what they want (e.g., submit to PDL).
It would be nice to get some confirmation that this worked.
For now you can check the aqms-pdl-app logfile:
retval = 1 ==> row inserted into pcs_state table
retavl = 0 ==> No row inserted - maybe because it's already there (e.g., you can't repeat it)

##### Note: 
Now that you can use the app to (re) submit the event into PDL, be sure
not to subsequently suck the event back in through PDL --> pdl2aqms. 

e.g., make sure --ignore-sources=XX is used on the line in the ProductClient .ini file
that triggers pdl2aqms so that pdl2aqms will ignore messages received from your network
code (XX).

 

###### Reload Table
Each table view has a Reload Table button, however, most of the time
you won't need to use it; the table will automatically be reloaded as needed.
For instance, if you make a new association in Assocevents
or change anything in the Filters settings, every table view (tab)
that depends on those settings will be reloaded the next time you
click on the tab.  The Reload Table buttons are there for the case
when you have the app open for a long time and some other process
not related to aqms-pdl-app has modified the tables and you want
to update your view.

###### Filtering

![new_GUI](docs/13.png)

By default, Limit MaxRows is selected.  The default value can be set by:
1. the cmd line (--maxrows 14)
2. In the config file (maxrows: 14)
3. will default to 14 if nothing set

The reason for this is that the app is not particularly fast, so if no filter is set,
it will load the entire Event/Origin/Netmag tables which can take some time.
Note that the filters are not exclusionary.  e.g., if you select a date range
and *also* limit max rows, the resulting views will show the most 
recent n=max_rows rows within the selected date range.

You must select Apply Filters for the changes to take effect.  When you do that 
the app will switch you over to the Event tab, showing an updated view with
the new filters applied.  The other tabs will auto reload the respective tables with the new
filters the first time you click on them after selecting Apply Filters.

##### The event table

While this app has been primarily designed to facilitate interation with
the Assocevents table (below), it also offers a view into the event
table:

http://127.0.0.1:5005  // or whatever port you are running this on

![Event table 1](docs/a.png)


Click on the event row(s) to open up the table of all Origin(s) for this
event.

![Event table 2](docs/b.png)

In the example above, evid 6 (auth=NC) has 1 origin while evid 11 (auth=NN)
has 2.

Click on an origin row to open up the table of all Netmag(s) for this
origin.

![Event table 3](docs/c.png)

Thus, this event (evid 6) has one origin (orid 6) and that origin has
two netmags (magid 6 and magid 11) associated with it, and magid 11
(local Mw magnitude) is the origin prefmag and also the event prefmag.


##### The assocevents table

The assocevents table is a new table (2021), designed to associate a
local (RSN) evid, with a foreign (non-RSN) evid imported via PDL.

http://127.0.0.1:5005  // or whatever port you are running this on

![Assocevents table 1](docs/1.png)

The example above shows the 2021 Antelope Valley earthquake after the
local (RSN=NC) RT1 solution has been automatically associated with 2
different PDL solutions: source=US and source=NN.

The table rows have background color (light v. dark) that alternates
with the local (RSN) evid.  e.g., in this case we have 2 PDL evids that
associate with the same RSN evid, hence this group together has the same
(light) background.  In general, the association will be 1-to-1 and the
background will alternate every other row.

Clicking on the Asssocevents row opens the dropdown Origins table (blue)
which shows all of the origins affiliated with either of the two events
in the Assocevents row (the local evid and the PDL evid):

![Assocevents table 2](docs/2.png)

Clicking on an Origin row opens the dropdown Netmags table which shows a
history of all the Netmags associated with this Origin:

![Assocevents table 3](docs/3.png)


Opening the PDL Origin (e.g., US) Netmag table shows that each PDL
Netmag has a Select Mag checkbox.

![Assocevents table 4](docs/4.png)

Checking this box and then selecting Update will trigger an attempt to
make the PDL magnitude (e.g., US Mww) the preferred magnitude for the
local evid:

![Assocevents table 5](docs/5.png)

![Assocevents table 6](docs/6.png)

Note how the local preferred magnitude is now the same (Mww = 6) as the
US Mww magnitude.

![Assocevents table 7](docs/7.png)

To Undo this and reset the local evid preferred magnitude to the
previous, preferred magnitude for the RSN, select Unselect mag + Update
and the preferred magnitude will revert to its previous value.  The only
difference in the database is that the local evid Event.version will be
incremented in the process.

**Note:** Setting/Unsetting the event preferred mag with the PDL magnitude
is a lightweight process. The event-->origin-->netmag trees in the
database are kept totally separate for RSN (non-PDL) and PDL streams.
The only place the 2 streams cross is in the Event.prefmag field, where
the local Event.prefmag and Eventprefmag.magid are now allowed to point
to a non-local Netmag magid.

**Note:** Setting the event prefmag is not guaranteed to work; it depends on
the local magprefpriority table.  Requesting Select Mag triggers a call
to the stored procedure

    epref.setprefmag_magtype(evid, magid)

where evid is a local evid and magid is the PDL Netmag magid we wish to
set.
This procedure will consult the magprefpriority table to decide if the
requested PDL magid has high enough priority to replace the current
event preferred magid.  If it doesn't have sufficient priority (and/or
if there is no priority rule for the PDL magid auth + type, etc), then
no change will be made.

###### Creating/Removing Assocevents

The Assocevents table is a simple table to associate local evid with
non-local evid for the same event (e.g., for 2 events that fall within
some specified threshold in time, location, etc).
Deleting (inserting) rows from (into) this table has no effect on the
other AQMS db tables; e.g., deleting a row will *not* delete either the
local evid or PDL evid Events, Origins, Netmags, etc.

To delete a row, simply select the Unassociate checkbox and Update:

![Assocevents table 8](docs/8.png)

![Assocevents table 9](docs/9.png)

To insert a new association, select the Associate link and enter the
local (evid1) and other (evid2) you wish to associate.  A text remark is
optional.  Then select Associate:

![Assocevents table 10](docs/10.png)

![Assocevents table 11](docs/11.png)



