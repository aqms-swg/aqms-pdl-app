#!/usr/bin/env python
# -*- coding: utf-8 -*-

from codecs import open
from os import path
from setuptools import setup, find_packages

here = path.abspath(path.dirname(__file__))

version='0.0.2'

'''
with open(path.join(here, 'README.rst'), encoding='utf-8') as readme_file:
    readme = readme_file.read()

with open(path.join(here, 'HISTORY.rst'), encoding='utf-8') as history_file:
    history = history_file.read().replace('.. :changelog:', '')
'''

requirements = [
    'aqms_pdl',
    'flask',
    'flask-wtf',
]

""" MTH: This works but package_data seems to be preferred/more reliable
import sys
import site
pkg_name = 'aqms_pdl_app'
py_version = '%s.%s' % (sys.version_info[0], sys.version_info[1])
path = 'lib/python%s/site-packages/%s' % (py_version, pkg_name)

data_dir = os.path.join(path, 'app/static/styles')

data_files = [(data_dir, ["aqms_pdl_app/app/static/styles/styles.css",
                          "aqms_pdl_app/app/static/styles/table_styles.css",
                          "aqms_pdl_app/app/static/styles/sorttable.js",
                          ]
             )]
"""

#package_data = {'aqms_pdl_app': ['app/static', 'app/templates',]}
#package_data = {'aqms_pdl_app': ['app/templates']}

setup(
    name='aqms-pdl-app',
    version=version,
    description="Small flask app to interact with AQMS postgres db tables",
    #long_description=readme + '\n\n' + history,
    author="Mike Hagerty",
    author_email='mhagerty@isti.com',
    url='https://gitlab.isti.com/mhagerty/flask-aqms',
    packages=find_packages(exclude=['contrib', 'docs', 'tests']),
    entry_points={
        'console_scripts':[
            'aqms-pdl-app=aqms_pdl_app.flask_aqms:main',
            #'flask-aqms=flask_aqms:main',
            ],
        },
    include_package_data=True,
    #package_data=package_data,
    #data_files = data_files,
    install_requires=requirements,
    license="MIT",
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        "Programming Language :: Python :: 2",
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.8',
    ],
    #test_suite='tests',
    #tests_require=test_requirements,
    #extras_require={ # MTH: eg, >pip install pkg[cli]
        #'cli': cli_requirements,
        #'develop': develop_requirements,
        #}
)
